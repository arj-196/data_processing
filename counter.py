# -*- coding: utf-8 -*-
import UniTextFR
from pymongo import MongoClient
import sys, traceback
import pickle
import mapper as mp
import resources as rs
from nltk.util import ngrams
from nltk.tag.stanford import StanfordNERTagger
import json
import re
import os


class Utils(object):
    @staticmethod
    def get_ngrams_set(tokens):
        grams = []
        for n in range(rs.MIN_LENGTH_GRAMS, rs.MAX_LENGTH_GRAMS + 1):
            grams.append(ngrams(tokens, n))

        return grams


class UniTextFRCollection(object):
    @staticmethod
    def _process_corpus(corpus):
        analyzer = UniTextFR.UniTextFR(corpus, on_sentences=False, no_duplicates=False)
        analyzer.clean_tolower()
        analyzer.clean_ponctuation()
        analyzer.clean_special_chars()
        # analyzer.extract_pos()
        # # analyzer.clean_numbers()
        # # analyzer.clean_html()
        # words_to_del = ['réponse', 'réponses', 'bonjour', '-']
        # analyzer.clean_words(words_to_del)  # TODO why it becoming a Series from Dataframe
        # analyzer.cleaned_corpus.to_csv("xdump.csv")

        return analyzer

    @staticmethod
    def _load_corpus(collection):
        client = MongoClient(rs.MONGOURL)
        db = client.bnp_ent
        col = db[collection]
        raw_corpus = [t['posts'] for t in
                      col.find({"posts": {"$exists": "true", "$not": {"$size": 0}}}, {"_id": 0, "posts": 1})]

        # taking only content data
        corpus = []
        for c1 in raw_corpus:
            t = ""
            for c2 in c1:
                if 'content' in c2:
                    t += c2['content'] + " "
            corpus.append(t)
        return corpus

    def create_analyzers(self, collections):
        print "Init Analyzers"
        analyzers = []
        for col in collections:
            print "---------", "Collection : ", col
            # analyze corpus
            analyzer = self._process_corpus(self._load_corpus(col))
            analyzers.append({"collection": col, "analyzer": analyzer})
        return analyzers


class SimpleWriter(object):
    def __init__(self):
        pass

    @staticmethod
    def write_categories(filename, data, ids_name):
        sep = '\t'
        newline = '\n'
        # empty file
        f = open(filename, "w")
        f.close()

        f = open(filename, "a")
        for key in data:
            row_string = ""
            try:
                row_string += str(key) + sep
                row_string += str(ids_name[key]) + sep
                for columns in data[key]:
                    d = data[key][columns]
                    if type(d) == list:
                        row_string += ' '.join(d) + sep
                    else:
                        row_string += str(d) + sep

                row_string += newline
                f.write(row_string.encode('utf8'))
            except:
                pass


class Counter(object):
    collections = None
    analyzers = None
    mappers = None

    def init_variables(self, analyzers, mappers):
        self.analyzers = analyzers
        self.mappers = mappers


class NounCounter(Counter):
    def run(self):
        print "Counter : Start Run"
        # run for all mappers
        for mapperobj in self.mappers:
            mapper = mapperobj['mapper']
            values = mapperobj['values']

            for analyzerObj in self.analyzers:
                analyzer = analyzerObj['analyzer']

                # iterate over all unique nouns
                for noun in analyzer.count_nouns:
                    w = noun[0]
                    count = noun[1]

                    if w in mapper.map:
                        for id_index in mapper.map[w]:
                            values[id_index]['value'] += count

                            # format additional variables
                            string = str(noun) + " "
                            if string not in values[id_index]['cause']:
                                values[id_index]['cause'].append(string)

            print "Storing results for ", mapperobj['category'], mapperobj['mapper'].map.keys()[0:10]
            # mapper 1
            # storing results
            pickle.dump(values, open("results/" + mapperobj['category'] + ".p", "wb"))
            SimpleWriter.write_categories("results/" + mapperobj['category'] + ".csv", values, mapper.ids_name)
        print "Counter : END Run"


class NGramCounter(Counter):
    # TODO add capability to build analyzers on the go
    def run(self, fileprefix, prebuilt_analyzers=True, collections=None):
        print "Counter : START Run"
        # run for all mappers
        for mapperobj in self.mappers:
            mapper = mapperobj['mapper']
            values = mapperobj['values']

            for analyzerObj in self.analyzers:
                analyzer = analyzerObj['analyzer']
                # iterate over all content
                for content in analyzer.cleaned_corpus.iterrows():
                    text = content[1].tolist()
                    tokens = []
                    if len(text) > 0:
                        tokens = text[0].split()  # splitting on whitespace

                    # get set of 2-4 length ngrams
                    ngram_set = Utils.get_ngrams_set(tokens)
                    for grams in ngram_set:
                        for gram in grams:
                            try:
                                # analyze gram
                                id_ngram = mp.NGramMapper.hash_ngram(gram)
                                if id_ngram in mapper.map:
                                    for id_index in mapper.map[id_ngram]:

                                        # increment respective gram_length score
                                        length_gram = mapper.gram_to_length_map[id_ngram]
                                        if length_gram in values[id_index]['gram_value']:
                                            values[id_index]['gram_value'][length_gram] += 1
                                        else:
                                            values[id_index]['gram_value'][length_gram] = 1

                                        # add ngram to respective gram_length list
                                        if length_gram in values[id_index]['cause']:
                                            if gram not in values[id_index]['cause'][length_gram]:
                                                values[id_index]['cause'][length_gram].append(gram)
                                        else:
                                            values[id_index]['cause'][length_gram] = [gram]

                                        # increment overall score
                                        values[id_index]['value'] += 1
                            except:
                                e = sys.exc_info()[0]
                                print ("ERROR GET_THREAD_FORUM_NAME %s", e)
                                traceback.print_exc(file=sys.stdout)
            # storing results
            pickle.dump(values, open("results/" + fileprefix + mapperobj['category'] + ".p", "wb"))
            self._dump_data("results/" + fileprefix + mapperobj['category'] + ".csv", values, mapper.ids_name)
        print "Counter : END Run"

    @staticmethod
    def _dump_data(filename, data, ids_name):
        sep = '\t'
        newline = '\n'
        # empty file
        f = open(filename, "w")
        f.close()

        f = open(filename, "a")
        # f.write(
        #     "id" + sep + "sector" + sep + "ngrams" + sep + "4gram score"
        #     + sep + "3gram score" + sep + "2gram score" + sep + "overall score" + newline)
        for key in data:
            row_string = ""
            try:
                row_string += str(key) + sep
                row_string += str(ids_name[key]) + sep
                row_string += str(data[key]['cause']) + sep

                if 4 in data[key]['gram_value']:
                    row_string += str(data[key]['gram_value'][4]) + sep
                else:
                    row_string += str(0) + sep

                if 3 in data[key]['gram_value']:
                    row_string += str(data[key]['gram_value'][3]) + sep
                else:
                    row_string += str(0) + sep

                if 2 in data[key]['gram_value']:
                    row_string += str(data[key]['gram_value'][2]) + sep
                else:
                    row_string += str(0) + sep

                row_string += str(data[key]['value']) + sep

                row_string += newline
                f.write(row_string.encode('utf8'))
            except UnicodeError:
                f.write(row_string)


class NERCounter(Counter):
    tagger = None
    tags = None
    tag_counts = {}
    iteration = 0
    words = {}

    def __init__(self):
        self.tagger = StanfordNERTagger(rs.stanford_rs_classifers, rs.stanford_rs_ner_jar)
        self.tags = {}
        self.tag_counts = {}

    def run(self, fileprefix):
        print "COUNTER NER : Start run"
        for analyzerObj in self.analyzers:
            self.iteration += 1
            analyzer = analyzerObj['analyzer']
            # analyzer.extract_pos()
            for w in analyzer.count_nouns:
                word = re.sub('[^0-9a-zA-Z]+', '', w[0])  # keep only alphanumeric
                count = w[1]

                if word not in self.tag_counts:
                    self.tag_counts[word] = count

            for w in analyzer.count_verbs:
                word = re.sub('[^0-9a-zA-Z]+', '', w[0])  # keep only alphanumeric
                count = w[1]

                if word not in self.tag_counts:
                    self.tag_counts[word] = count

            for w in analyzer.count_adjs:
                word = re.sub('[^0-9a-zA-Z]+', '', w[0])  # keep only alphanumeric
                count = w[1]

                if word not in self.tag_counts:
                    self.tag_counts[word] = count

            encoded_tokens = []
            # take only encoded tokens
            for token in self.tag_counts.keys():
                try:
                    encoded_tokens.append(token.encode('latin-1', 'ignore'))
                except:
                    pass

            try:
                tagged = self.tagger.tag(encoded_tokens)
                for tag in tagged:
                    # list of tags per NER
                    if tag[1] in self.tags:
                        if tag[0] not in self.tags[tag[1]]:
                            self.tags[tag[1]].append(tag[0])
                    else:
                        self.tags[tag[1]] = [tag[0]]
            except:
                pass

            with open('results/ner_counter_' + fileprefix + '_' + str(self.iteration) + '.json', 'w') as outfile:
                json.dump(self.tags, outfile)
        print "COUNTER NER : End run"

    def export_results(self, filename):
        print "COUNTER: Export results"
        sep = '\t'
        newline = '\n'
        # empty file
        f = open('results/ner_counter_' + filename + '.csv', "w")
        f.close()

        f = open('results/ner_counter_' + filename + '.csv', "a")
        for key in self.tags:
            for tag in self.tags[key]:
                string = ""
                string += key + sep
                string += tag + sep
                string += str(self.tag_counts[tag]) + newline
                f.write(string)
        f.close()


class LocationCounter(Counter):
    values = None
    mapper = None
    iteration = 0

    def run(self, fileprefix, save_data=False):
        print "LOCATIONCOUNTER : Start run"
        for mapperobj in self.mappers:
            self.iteration += 1
            self.mapper = mapperobj['mapper']
            self.values = mapperobj['values']
            iteration = 0

            for analyzer in self.analyzers:

                for w in self.mapper.map:
                    try:
                        ptn = ""
                        # breaking creating pattern from
                        a = re.split(' |-', w)  # split on ' ' or '-'
                        for i, v in enumerate(a):
                            ptn += v
                            if i != len(a) - 1:
                                ptn += '.'

                        print "keyword", w, "created pattern: ", ptn

                        if ptn != "":
                            indexes = analyzer.Corpus.str.contains(re.compile('\\b' + ptn + '\\b', re.IGNORECASE))
                            count = len(indexes[indexes == True])
                            self.values[self.mapper.map[w][0]]['value'] = count

                            print "got", w, len(indexes[indexes == True])

                            if save_data:
                                self._save_data(analyzer[indexes], w, count)
                    except:
                        e = sys.exc_info()[0]
                        print ("ERROR %s", e)
                        traceback.print_exc(file=sys.stdout)

        print "LOCATIONCOUNTER : End run"

    @staticmethod
    def _save_data(dataframe, keyword, count):
        SEP = '\t'
        NEWLINE = '\n'
        if count > 0:
            f = open('results/location_counter_data_verbatims_' + keyword + '.csv', 'w')
            for d in dataframe.iterrows():
                try:
                    s = d[1].Corpus.encode("utf-8")
                    s = s.replace('\n', ' ').replace('\t', ' ')
                    s += NEWLINE
                    f.write(s)
                except:
                    pass

    def export_results(self, filename):
        print "LOCATIONCOUNTER: Export results"
        sep = '\t'
        newline = '\n'
        # empty file
        f = open('results/location_counter_' + filename + '.csv', "w")
        f.close()

        f = open('results/location_counter_' + filename + '.csv', "a")
        for key in self.values:
            try:
                string = ""
                string += key + sep
                string += self.mapper.ids_name[key] + sep
                string += str(self.values[key]['value']) + sep
                string += newline
                f.write(string)
            except:
                pass


class WordCounter(Counter):
    iteration = 0
    nouns = {}
    verbs = {}
    adj = {}

    def run(self, fileprefix):
        print "WORDCOUNTER : Start run"
        for analyzerObj in self.analyzers:
            analyzer = analyzerObj['analyzer']
            self.iteration += 1

            for w in analyzer.count_nouns:
                word = re.sub('[^0-9a-zA-Z]+', '', w[0])  # keep only alphanumeric
                if word not in self.nouns:
                    self.nouns[word] = w[1]

            for w in analyzer.count_verbs:
                word = re.sub('[^0-9a-zA-Z]+', '', w[0])  # keep only alphanumeric
                if word not in self.verbs:
                    self.verbs[word] = w[1]

            for w in analyzer.count_adjs:
                word = re.sub('[^0-9a-zA-Z]+', '', w[0])  # keep only alphanumeric
                if word not in self.adj:
                    self.adj[word] = w[1]

            with open('results/word_counter_' + fileprefix + '_nouns_' + str(self.iteration) + '.json', 'w') as outfile:
                json.dump(self.nouns, outfile)
            with open('results/word_counter_' + fileprefix + '_verbs_' + str(self.iteration) + '.json', 'w') as outfile:
                json.dump(self.verbs, outfile)
            with open('results/word_counter_' + fileprefix + '_adjs_' + str(self.iteration) + '.json', 'w') as outfile:
                json.dump(self.adj, outfile)
        print "WORDCOUNTER : End run"

    def export_results(self, filename):
        print "WORDCOUNTER: Export results"
        sep = '\t'
        newline = '\n'
        # empty file
        f = open('results/word_counter_' + filename + '.csv', "w")
        f.close()

        f = open('results/word_counter_' + filename + '.csv', "a")
        for key in self.nouns:
            string = ""
            string += "NOUN" + sep
            string += key + sep
            string += str(self.nouns[key]) + sep
            string += newline
            f.write(string)

        for key in self.verbs:
            string = ""
            string += "VERB" + sep
            string += key + sep
            string += str(self.verbs[key]) + sep
            string += newline
            f.write(string)

        for key in self.adj:
            string = ""
            string += "ADJ" + sep
            string += key + sep
            string += str(self.adj[key]) + sep
            string += newline
            f.write(string)
        f.close()


def backup_data(o):
    _type = None
    s = True
    if isinstance(o, LocationCounter):
        _type = "lc"
    else:
        print "BACKUP: ERROR: Unkown type "
        s = False

    if s:
        # create directory strucutre
        if not os.path.exists('tmp'):
            os.makedirs('tmp')

        if not os.path.exists('tmp/' + _type):
            os.makedirs('tmp/' + _type)

        # save data
        if isinstance(o, LocationCounter):
            root = 'tmp/' + _type + '/'
            pickle.dump(o.mapper.map, open(root + 'mapper.map', 'wb'))
            pickle.dump(o.mapper.ids_name, open(root + 'mapper.ids_name', 'wb'))
            pickle.dump(o.values, open(root + 'mapper.values', 'wb'))


def recover_stored_data(_type):
    # TODO
    root = 'tmp/' + _type + '/'
    if _type == 'lc':
        c = LocationCounter()
