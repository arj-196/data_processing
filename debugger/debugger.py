from datetime import datetime
import os
import time
startTime = datetime.now()
start = time.time()

SEP = ','
NEWLINE = '\n'


class Debugger(object):
    var_files = {}
    num_var = 1
    DIR = None
    session = None

    def __init__(self, DIR, name):
        self.session = name + "." + startTime.strftime("%d_%m_%Y.%H_%M_%S.")
        self.DIR = DIR
        self._verify_configurations()

    def __del__(self):
        self.close_var_files()

    def _verify_configurations(self):
        if not os.path.exists(self.DIR):
            os.makedirs(self.DIR)

    def log(self, variable, message):
        if variable not in self.var_files:
            self._create_log_file(variable)
        # write to log
        self._write_log(self.var_files[variable], variable, message)

    def _create_log_file(self, var):
        self.var_files[var] = open(self.DIR + self.session + "_" + var + ".csv", 'w')

    def _write_log(self, _file, var, message):
        string = var + SEP
        string += self._get_time_passed() + SEP + SEP
        string += message + NEWLINE
        _file.write(string)

    def close_var_files(self):
        for var in self.var_files:
            self.var_files[var].close()

    @staticmethod
    def _get_time_passed():
        seconds = time.time()-start
        sign_string = '-' if seconds < 0 else ''
        seconds = abs(int(seconds))
        days, seconds = divmod(seconds, 86400)
        hours, seconds = divmod(seconds, 3600)
        minutes, seconds = divmod(seconds, 60)
        return '%s %dd %dh %dm %ds' % (sign_string, days, hours, minutes, seconds)

#
# debugger = Debugger("H:\\PDS\\BNP\\data_processing\\logs\\", "test")
# debugger.log("var_1", "message 1")
# debugger.log("var_2", "message 4")
#
