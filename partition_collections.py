from pymongo import MongoClient
import re
from sets import Set
from datetime import datetime
import atexit
startTime = datetime.now()
import sys, traceback


def move_item(_id, thread, col_name):
    if not db[col_name].find_one({"_id": _id}):
        db[col_name].insert(thread)  # insert to new
        old_col.remove({"_id": _id})  # remove from master
        # print "move", _id, "to", col_name


def main():
    print "PARTITIONING START", datetime.now() - startTime
    iteration = 0
    for search_domain in domains:
        for thread in old_col.find({"domain": search_domain}):
            iteration += 1
            domain = thread['domain']
            cur_url = thread['url']

            try:
                url_patterns = valid_urls[domain]
                for ptn in url_patterns:
                    r = re.findall(ptn, cur_url)
                    if r:
                        # valid url
                        _id = thread["_id"]
                        col_name = None
                        for col_set in new_col:
                            if domain in new_col[col_set]:
                                col_name = col_set

                        if col_name:
                            move_item(_id, thread, col_name)
            except:
                e = sys.exc_info()[0]
                print ("ERROR GET_THREAD_FORUM_NAME %s", e)
                traceback.print_exc(file=sys.stdout)
            # test break
            # if iteration > 5:
            #     print "EXIT PREMATURE", exit(196)


def before_exit():
    print "PARTITIONING EXIT", datetime.now() - startTime

# at exist handler
atexit.register(before_exit)

if __name__ == '__main__':
    #
    # 1 ---------------
    client = MongoClient("localhost")
    # client = MongoClient("mongodb://51.254.223.26:27017")
    db = client.posts  # db
    old_col = db.master  # master collection
    new_col = {
        'intrapreneuriat': Set(
            ["forum.psychologies.com",
             "droit-finances.commentcamarche.net",
             "keljob.com",
             "forum.aufeminin.com"]
        ),
        'reconversions': Set(
            ["forum.aufeminin.com"]
        )
    }
    domains = [
        # "forum.psychologies.com",
        # "forum.aufeminin.com",
        "droit-finances.commentcamarche.net",
        # "developpez.net",
        # "keljob.com"
    ]

    valid_urls = {
        "forum.psychologies.com": ['/Travail-carriere-reconversion/'],
        "forum.aufeminin.com": ['/carriere1/', '/f816/'],
        "droit-finances.commentcamarche.net": ['.*'],
        "developpez.net": [
            # '/emploi-etudes-informatique/',
            'd1542761-2/general-developpement/debats-developpement-best-of/'],
        "keljob.com": ["/gerer-sa-carriere/"]

    }

    #
    # 2 ---------------
    # client = MongoClient("mongodb://51.254.223.26:27017")
    # db = client.posts
    #
    # domains = ["droit-finances.commentcamarche.net"]
    #
    # old_col = client.posts.intrapreneuriat
    # new_col = {
    #     'master': Set(["droit-finances.commentcamarche.net"])
    # }
    #
    # valid_urls = {
    #     "droit-finances.commentcamarche.net": ['.*']
    # }


    #
    main()
