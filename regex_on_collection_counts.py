import counter as dp
import resources as rs
import mapper as mp
from pymongo import MongoClient
import re
from sets import Set
from datetime import datetime
import sys, traceback
import pickle

startTime = datetime.now()


class RegexCounter(object):
    clean_cache_interval = 20000
    debug = ""
    iteration = 0
    mapper = None

    def __init__(self, mapper):
        self.client = MongoClient(rs.MONGOURL)
        self.mapper = mapper

    def __del__(self):
        print "REGEX COUNTER : stopped at ", self.debug, datetime.now() - startTime
        self.clean_cache()
        print

    @staticmethod
    def clean_sentence(sentence):
        # to lower
        s = sentence.lower()
        # remove all puntutations
        for p in rs.puntuations:
            s = s.replace(p, "")
        # remove multiple spaces
        re.sub(' +', ' ', s)
        return s

    def run(self, collections):
        print "REGEX COUNTER : Start run"
        db = self.client.bnp_ent
        for collection in collections:
            print "--collection", collection

            for thread in db[collection].find({"posts": {"$exists": "true", "$not": {"$size": 0}}},
                                              {"_id": 0, "posts": 1}, no_cursor_timeout=True):
                if 'posts' in thread:
                    for post in thread['posts']:
                        self.debug = collection + " " + str(self.iteration)
                        self.iteration += 1
                        if 'content' in post:
                            content = self.clean_sentence(post['content'])
                            self._analyze_text(content)

                # clean cache at interval
                if self.iteration % self.clean_cache_interval == 0:
                    self.clean_cache()

                # if self.iteration > 20:
                #     print "REGEX COUNTER: Exit Premature"
                #     break
        print "REGEX COUNTER : End run"

    def _analyze_text(self, text):
        for p in self.mapper.map:
            ptn = re.compile('.*' + p + '.*', re.IGNORECASE)
            if re.search(ptn, text):
                for _id in self.mapper.map[p]:
                    self.mapper.set_ids[_id]['value'] += 1


    def clean_cache(self):
        print "REGEX COUNTER : Clear cache", datetime.now() - startTime
        pickle.dump(self.mapper.set_ids, open('tmp/regex_on_col.set_ids.p', 'wb'))

    def export_results(self, filename):
        sep = '\t'
        newline = '\n'
        print "REGEX COUNTER : Export results"
        f = open("results/regex_counter_" + filename + '.csv', 'w')
        f.close()
        f = open("results/regex_counter_" + filename + '.csv', 'a')
        for key in self.mapper.set_ids:
            try:
                string = ""
                string += key + sep
                string += self.mapper.ids_name[key] + sep
                string += str(self.mapper.set_ids[key]['value']) + sep
                string += newline
                f.write(string)
            except:
                pass


def main(filename):
    mapper = mp.MapperCollection().create_mappers(mp.StringMapper, rs.categories, rs.categories_csv_mapper,
                                                  rs.categories_to_resource_map, split=False)
    counter = RegexCounter(mapper[0]['mapper'])
    counter.run(['bnpdb', 'bnpdb2', 'bnpdb3', 'bnpdb4', 'bnpdb5'])
    # counter.run(['bnpdbtest2'])
    counter.export_results(filename)


if __name__ == '__main__':
    argv = sys.argv
    if argv[1] == 'calculate':
        main(argv[2])
