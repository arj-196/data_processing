# -*- coding: utf-8 -*-

# Dependencies
from pattern.text.fr import parse, split, sentiment, parsetree, ngrams
from pattern.text.fr import conjugate, tag
from pattern.text.fr import INFINITIVE, PRESENT, PAST, SG, SUBJUNCTIVE, PERFECTIVE
import pandas as pd


class UniText(object):
    corpus = None  # one column data frame
    cleaned_corpus = None
    nouns = None  # column with nounds
    verbs = None
    adjs = None
    tokens = None
    count_nouns = None
    count_verbs = None
    count_adjs = None
    ngrams = []

    def __init__(self, corpus, on_sentences=True, no_duplicates=True):
        # @todo !!!!! This should be language specific

        # Transform the corpus into data frame
        # @todo create cleaned_corpus
        # If the corpus is a list
        if isinstance(corpus, list):
            if on_sentences:
                # initialize a list of all phrases
                all_phrases = []
                # for each element of the list
                for element in corpus:
                    # parse the element
                    q = parsetree(element, tokenize=True)
                    for n in q.sentences:
                        phrase_as_string = n.string  # convrt each sentence to string
                        all_phrases.append(str(phrase_as_string.encode("utf-8")))  # append each sentence to the list
                self.corpus = pd.DataFrame({'Corpus': all_phrases})

                if no_duplicates:
                    before = len(self.corpus.index)
                    print("Initial size: " + str(before))
                    self.corpus = self.corpus.drop_duplicates('Corpus')
                    after = len(self.corpus.index)
                    print("Final size: " + str(after))
                    print("Removed " + str(before - after) + " duplicates")

                else:
                    print("No duplicates removed")
                # Initialize cleaned corpus
                self.cleaned_corpus = self.corpus
                print('Created a corpus (each sentence separately)')
            if not on_sentences:
                # corpus is created with sentence extraction
                self.corpus = pd.DataFrame({'Corpus': corpus})

                if no_duplicates:
                    before = len(self.corpus.index)
                    print("Initial size: " + str(before))
                    self.corpus = self.corpus.drop_duplicates('Corpus')
                    after = len(self.corpus.index)
                    print("Final size: " + str(after))
                    print("Removed " + str(before - after) + " duplicates")

                else:
                    print("No duplicates removed")
                # Initialize cleaned corpus
                self.cleaned_corpus = self.corpus
                print('Created a corpus without sentence separation')
        elif isinstance(corpus, str):
            # Extract sentences from corpus
            # @todo STEP 2
            pass
        else:
            print('The format of the data must be a list or string')
