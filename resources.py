# -*- coding: utf-8 -*-

stop_words = {
    'en':
        [
            'a', 'able', 'about', 'across', 'after', 'all', 'almost', 'also', 'am', 'among', 'an', 'and', 'any', 'are',
            'as', 'at', 'be', 'because', 'been', 'but', 'by', 'can', 'cannot', 'could', 'dear', 'did', 'do', 'does',
            'either', 'else', 'ever', 'every', 'for', 'from', 'get', 'got', 'had', 'has', 'have', 'he', 'her', 'hers',
            'him', 'his', 'how', 'however', 'i', 'if', 'in', 'into', 'is', 'it', 'its', 'just', 'least', 'let', 'like',
            'likely', 'may', 'me', 'might', 'most', 'must', 'my', 'neither', 'no', 'nor', 'not', 'of', 'off', 'often',
            'on', 'only', 'or', 'other', 'our', 'own', 'rather', 'said', 'say', 'says', 'she', 'should', 'since', 'so',
            'some', 'than', 'that', 'the', 'their', 'them', 'then', 'there', 'these', 'they', 'this', 'tis', 'to',
            'too', 'twas', 'us', 'wants', 'was', 'we', 'were', 'what', 'when', 'where', 'which', 'while', 'who', 'whom',
            'why', 'will', 'with', 'would', 'yet', 'you', 'your',

            "\'d", " \'ll", " \'m", " \'re", " \'s", " \'t", " n\'t",
            " \'ve", " a", " aboard", " about", " above", " across", " after", " again", " against", " all", " almost",
            " alone", " along", " alongside", " already", " also", " although", " always", " am", " amid", " amidst",
            " among", " amongst", " an", " and", " another", " anti", " any", " anybody", " anyone", " anything",
            " anywhere", " are", " area", " areas", " aren't", " around", " as", " ask", " asked", " asking", " asks",
            " astride", " at", " aught", " away", " back", " backed", " backing", " backs", " bar", " barring", " be",
            " became", " because", " become", " becomes", " been", " before", " began", " behind", " being", " beings",
            " below", " beneath", " beside", " besides", " best", " better", " between", " beyond", " big", " both",
            " but", " by", " came", " can", " can't", " cannot", " case", " cases", " certain", " certainly", " circa",
            " clear", " clearly", " come", " concerning", " considering", " could", " couldn't", " daren't", " despite",
            " did", " didn't", " differ", " different", " differently", " do", " does", " doesn't", " doing", " don't",
            " done", " down", " down", " downed", " downing", " downs", " during", " each", " early", " either", " end",
            " ended", " ending", " ends", " enough", " even", " evenly", " ever", " every", " everybody", " everyone",
            " everything", " everywhere", " except", " excepting", " excluding", " face", " faces", " fact", " facts",
            " far", " felt", " few", " fewer", " find", " finds", " first", " five", " following", " for", " four",
            " from", " full", " fully", " further", " furthered", " furthering", " furthers", " gave", " general",
            " generally", " get", " gets", " give", " given", " gives", " go", " goes", " going", " good", " goods",
            " got", " great", " greater", " greatest", " group", " grouped", " grouping", " groups", " had", " hadn't",
            " has", " hasn't", " have", " haven't", " having", " he", " he'd", " he'll", " he's", " her", " here",
            " here's", " hers", " herself", " high", " high", " high", " higher", " highest", " him", " himself",
            " his", " hisself", " how", " how's", " however", " i", " i'd", " i'll", " i'm", " i've", " idem", " if",
            " ilk", " important", " in", " including", " inside", " interest", " interested", " interesting",
            " interests", " into", " is", " isn't", " it", " it's", " its", " itself", " just", " keep", " keeps",
            " kind", " knew", " know", " known", " knows", " large", " largely", " last", " later", " latest", " least",
            " less", " let", " let's", " lets", " like", " likely", " long", " longer", " longest", " made", " make",
            " making", " man", " many", " may", " me", " member", " members", " men", " might", " mightn't", " mine",
            " minus", " more", " most", " mostly", " mr", " mrs", " much", " must", " mustn't", " my", " myself",
            " naught", " near", " necessary", " need", " needed", " needing", " needn't", " needs", " neither",
            " never", " new", " new", " newer", " newest", " next", " no", " nobody", " non", " none", " noone", " nor",
            " not", " nothing", " notwithstanding", " now", " nowhere", " number", " numbers", " of", " off", " often",
            " old", " older", " oldest", " on", " once", " one", " oneself", " only", " onto", " open", " opened",
            " opening", " opens", " opposite", " or", " order", " ordered", " ordering", " orders", " other", " others",
            " otherwise", " ought", " oughtn't", " our", " ours", " ourself", " ourselves", " out", " outside", " over",
            " own", " part", " parted", " parting", " parts", " past", " pending", " per", " perhaps", " place",
            " places", " plus", " point", " pointed", " pointing", " points", " possible", " present", " presented",
            " presenting", " presents", " problem", " problems", " put", " puts", " quite", " rather", " really",
            " regarding", " right", " right", " room", " rooms", " round", " said", " same", " save", " saw", " say",
            " says", " second", " seconds", " see", " seem", " seemed", " seeming", " seems", " seen", " sees", " self",
            " several", " shall", " shan't", " she", " she'd", " she'll", " she's", " should", " shouldn't", " show",
            " showed", " showing", " shows", " side", " sides", " since", " small", " smaller", " smallest", " so",
            " some", " somebody", " someone", " something", " somewhat", " somewhere", " state", " states", " still",
            " still", " such", " suchlike", " sundry", " sure", " take", " taken", " than", " that", " that's", " the",
            " thee", " their", " theirs", " them", " themselves", " then", " there", " there's", " therefore", " these",
            " they", " they'd", " they'll", " they're", " they've", " thine", " thing", " things", " think", " thinks",
            " this", " those", " thou", " though", " thought", " thoughts", " three", " through", " throughout",
            " thus", " thyself", " till", " to", " today", " together", " too", " took", " tother", " toward",
            " towards", " turn", " turned", " turning", " turns", " twain", " two", " under", " underneath", " unless",
            " unlike", " until", " up", " upon", " us", " use", " used", " uses", " various", " versus", " very",
            " via", " vis-a-vis", " want", " wanted", " wanting", " wants", " was", " wasn't", " way", " ways", " we",
            " we'd", " we'll", " we're", " we've", " well", " wells", " went", " were", " weren't", " what", " what's",
            " whatall", " whatever", " whatsoever", " when", " when's", " where", " where's", " whereas", " wherewith",
            " wherewithal", " whether", " which", " whichever", " whichsoever", " while", " who", " who's", " whoever",
            " whole", " whom", " whomever", " whomso", " whomsoever", " whose", " whosoever", " why", " why's", " will",
            " with", " within", " without", " won't", " work", " worked", " working", " works", " worth", " would",
            " wouldn't", " ye", " year", " years", " yet", " yon", " yonder", " you", " you'd", " you'll", " you're",
            " you've", " you-all", " young", " younger", " youngest", " your", " yours", " yourself", "yourselves"
        ],

    'fr':
        [
            'au', 'aux', 'avec', 'ce', 'ces', 'dans', 'de', 'des', 'du', 'elle', 'en', 'et', 'eux', 'il', 'je', 'la',
            'le',
            'leur', 'lui', 'ma', 'mais', 'me', 'même', 'mes', 'moi', 'mon', 'ne', 'nos', 'notre', 'nous', 'on', 'ou',
            'par', 'pas', 'pour', 'qu', 'que', 'qui', 'sa', 'se', 'ses', 'son', 'sur', 'ta', 'te', 'tes', 'toi', 'ton',
            'tu', 'un', 'une', 'vos', 'votre', 'vous', 'c', 'd', 'j', 'l', 'à', 'm', 'n', 's', 't', 'y', 'été', 'étée',
            'étées', 'étés', 'étant', 'suis', 'es', 'est', 'sommes', 'être', 'êtes', 'sont', 'serai', 'seras', 'sera',
            'serons',
            'serez', 'seront', 'serais', 'serait', 'serions', 'seriez', 'seraient', 'étais', 'était', 'étions', 'étiez',
            'étaient', 'fus', 'fut', 'fûmes', 'fûtes', 'furent', 'sois', 'soit', 'soyons', 'soyez', 'soient', 'fusse',
            'fusses', 'fût', 'fussions', 'fussiez', 'fussent', 'ayant', 'eu', 'eue', 'eues', 'eus', 'ai', 'as', 'avoir',
            'avons', 'faut', 'veut',
            'avez', 'ont', 'aurai', 'auras', 'aura', 'aurons', 'aurez', 'auront', 'aurais', 'aurait', 'aurions',
            'auriez',
            'auraient', 'avais', 'avait', 'avions', 'aviez', 'avaient', 'eut', 'eûmes', 'eûtes', 'eurent', 'aie',
            'aies',
            'ait', 'ayons', 'ayez', 'aient', 'eusse', 'eusses', 'eût', 'eussions', 'eussiez', 'eussent', 'ceci', 'cela',
            'celà', 'cet', 'cette', 'ici', 'ils', 'les', 'leurs', 'quel', 'quels', 'quelle', 'quelles', 'sans', 'soi'
            , 'a', 'si', 'fait', 'quelques', 'bonjour', 'fois', 'depuis', 'plus', 'ça', 'après', 'peut', 'contre',
            'ceux'

            , 'coutumiermessages', 'celui', 'faire', 'tout', 'ans', 'toujours', 'ca', 'très', 'tant', 'aussi', 'comme'
            , 'oui', 'com', 'comme', 'alors', 'peux', 'doivent', 'pense', 'merci', 'car', 'fais', 'fait', 'voir', 'site'
            , 'dit', 'dite', 'dire', 'forum', 'tiens', 'dont', 'la', 'les', 'le', 'vers', 'voit', 'dès', 'tu', 'il'
            , 'entrepreneur', 'donc', 'écrit', 'tous', 'quoi', 'mois', 'hui', 'dois', 'avant', 'www', 'afin', 'bcp',
            'font'
            , 'etc', 'chaque', 'quelqu', 'enfin', 'quand', 'où', 'lors', 'souhait', 'moins', 'cais', 'va', 'dejà',
            'vais'
            , 'bien', 'autre', 'viens', 'toute', 'mettre', 'jamais', 'voila', 'comment', 'dis', 'deux', 'mail'
            , 'bon', 'sais', 'veux', 'doit', 'juste', 'temps', 'lire', 'trouve', 'aime', 'venir', 'non', 'cas', 'peu'
            , 'psy', 'vite', 'là', 'vois', 'sais', 'cas', 'fin', 'seul', 'entre', 'savoir', 'herbemessages', 'vraiment'
            , 'encore', 'etre', 'puis', 'effet', 'sites', 'mieux', 'voilà', 'ds', 'ex', 'ouvrir', 'service', 'vis'
            , 'cours', 'class',

            "Ap.", " Apr.", " GHz", " MHz", " USD", " a", " afin", " ah", " ai", " aie", " aient", " aies", " ait",
            " alors", " après", " as", " attendu", " au", " au-delà", " au-devant", " aucun", " aucune", " aucuns",
            " audit", " auprès", " auquel", " aura", " aurai", " auraient", " aurais", " aurait", " auras", " aurez",
            " auriez", " aurions", " aurons", " auront", " aussi", " autour", " autre", " autres", " autrui", " aux",
            " auxdites", " auxdits", " auxquelles", " auxquels", " avaient", " avais", " avait", " avant", " avec",
            " avez", " aviez", " avions", " avoir", " avons", " ayant", " ayez", " ayons", " b", " bah", " banco",
            " ben", " bien", " bon", " bé", " c", " c'", " c'est", " c'était", " car", " ce", " ceci", " cela",
            " celle", " celle-ci", " celle-là", " celles", " celles-ci", " celles-là", " celui", " celui-ci",
            " celui-là", " celà", " cent", " cents", " cependant", " certain", " certaine", " certaines", " certains",
            " ces", " cet", " cette", " ceux", " ceux-ci", " ceux-là", " cf.", " cg", " cgr", " chacun", " chacune",
            " chaque", " chez", " ci", " cinq", " cinquante", " cinquante-cinq", " cinquante-deux", " cinquante-et-un",
            " cinquante-huit", " cinquante-neuf", " cinquante-quatre", " cinquante-sept", " cinquante-six",
            " cinquante-trois", " cl", " cm", " cm²", " comme", " comment", " contre", " d", " d'", " d'après", " d'un",
            " d'une", " dans", " de", " dedans", " dehors", " depuis", " derrière", " des", " desdites", " desdits",
            " desquelles", " desquels", " deux", " devant", " devers", " devrait", " dg", " différentes", " différents",
            " divers", " diverses", " dix", " dix-huit", " dix-neuf", " dix-sept", " dl", " dm", " doit", " donc",
            " dont", " dos", " douze", " droite", " du", " dudit", " duquel", " durant", " dès", " début", " déjà",
            " e", " eh", " elle", " elles", " en", " en-dehors", " encore", " enfin", " entre", " envers", " es",
            " essai", " est", " et", " eu", " eue", " eues", " euh", " eurent", " eus", " eusse", " eussent", " eusses",
            " eussiez", " eussions", " eut", " eux", " eûmes", " eût", " eûtes", " f", " fait", " faites", " fi",
            " flac", " fois", " font", " force", " fors", " furent", " fus", " fusse", " fussent", " fusses",
            " fussiez", " fussions", " fut", " fûmes", " fût", " fûtes", " g", " gr", " h", " ha", " han", " haut",
            " hein", " hem", " heu", " hg", " hl", " hm", " hm³", " holà", " hop", " hormis", " hors", " huit", " hum",
            " hé", " i", " ici", " il", " ils", " j", " j'", " j'ai", " j'avais", " j'étais", " jamais", " je",
            " jusqu'", " jusqu'au", " jusqu'aux", " jusqu'à", " jusque", " juste", " k", " kg", " km", " km²", " l",
            " l'", " l'autre", " l'on", " l'un", " l'une", " la", " laquelle", " le", " lequel", " les", " lesquelles",
            " lesquels", " leur", " leurs", " lez", " lors", " lorsqu'", " lorsque", " lui", " là", " lès", " m", " m'",
            " ma", " maint", " mainte", " maintenant", " maintes", " maints", " mais", " malgré", " me", " mes", " mg",
            " mgr", " mil", " mille", " milliards", " millions", " mine", " ml", " mm", " mm²", " moi", " moins",
            " mon", " mot", " moyennant", " mt", " m²", " m³", " même", " mêmes", " n", " n'", " n'avait", " n'y",
            " ne", " neuf", " ni", " nommés", " non", " nonante", " nonobstant", " nos", " notre", " nous", " nouveaux",
            " nul", " nulle", " nº", " néanmoins", " o", " octante", " oh", " on", " ont", " onze", " or", " ou",
            " outre", " où", " p", " par", " par-delà", " parbleu", " parce", " parmi", " parole", " pas", " passé",
            " pendant", " personne", " personnes", " peu", " peut", " pièce", " plupart", " plus", " plus_d'un",
            " plus_d'une", " plusieurs", " pour", " pourquoi", " pourtant", " pourvu", " près", " puisqu'", " puisque",
            " q", " qu", " qu'", " qu'elle", " qu'elles", " qu'il", " qu'ils", " qu'on", " quand", " quant",
            " quarante", " quarante-cinq", " quarante-deux", " quarante-et-un", " quarante-huit", " quarante-neuf",
            " quarante-quatre", " quarante-sept", " quarante-six", " quarante-trois", " quatorze", " quatre",
            " quatre-vingt", " quatre-vingt-cinq", " quatre-vingt-deux", " quatre-vingt-dix", " quatre-vingt-dix-huit",
            " quatre-vingt-dix-neuf", " quatre-vingt-dix-sept", " quatre-vingt-douze", " quatre-vingt-huit",
            " quatre-vingt-neuf", " quatre-vingt-onze", " quatre-vingt-quatorze", " quatre-vingt-quatre",
            " quatre-vingt-quinze", " quatre-vingt-seize", " quatre-vingt-sept", " quatre-vingt-six",
            " quatre-vingt-treize", " quatre-vingt-trois", " quatre-vingt-un", " quatre-vingt-une", " quatre-vingts",
            " que", " quel", " quelle", " quelles", " quelqu'", " quelqu'un", " quelqu'une", " quelque", " quelques",
            " quelques-unes", " quelques-uns", " quels", " qui", " quiconque", " quinze", " quoi", " quoiqu'",
            " quoique", " r", " revoici", " revoilà", " rien", " s", " s'", " sa", " sans", " sauf", " se", " seize",
            " selon", " sept", " septante", " sera", " serai", " seraient", " serais", " serait", " seras", " serez",
            " seriez", " serions", " serons", " seront", " ses", " seulement", " si", " sien", " sinon", " six", " soi",
            " soient", " sois", " soit", " soixante", " soixante-cinq", " soixante-deux", " soixante-dix",
            " soixante-dix-huit", " soixante-dix-neuf", " soixante-dix-sept", " soixante-douze", " soixante-et-onze",
            " soixante-et-un", " soixante-et-une", " soixante-huit", " soixante-neuf", " soixante-quatorze",
            " soixante-quatre", " soixante-quinze", " soixante-seize", " soixante-sept", " soixante-six",
            " soixante-treize", " soixante-trois", " sommes", " son", " sont", " sous", " soyez", " soyons", " suis",
            " suite", " sujet", " sur", " sus", " t", " t'", " ta", " tacatac", " tandis", " te", " tel", " telle",
            " tellement", " telles", " tels", " tes", " toi", " ton", " toujours", " tous", " tout", " toute",
            " toutefois", " toutes", " treize", " trente", " trente-cinq", " trente-deux", " trente-et-un",
            " trente-huit", " trente-neuf", " trente-quatre", " trente-sept", " trente-six", " trente-trois", " trois",
            " trop", " très", " tu", " u", " un", " une", " unes", " uns", " v", " valeur", " vers", " via", " vingt",
            " vingt-cinq", " vingt-deux", " vingt-huit", " vingt-neuf", " vingt-quatre", " vingt-sept", " vingt-six",
            " vingt-trois", " vis-à-vis", " voici", " voie", " voient", " voilà", " vont", " vos", " votre", " vous",
            " vu", " w", " x", " y", " y'", " z", " zéro", " à", " ç'", " ça", " ès", " étaient", " étais", " était",
            " étant", " état", " étiez", " étions", " été", " étée", " étées", " étés", " êtes", " être", "ô",
            "class0", "class1", "class2"

        ]
}

puntuations = [
    ',',
    '.',
    '!',
    '?',
    ';',
    ':',
    '\"',
    '\'',
    '-',
    '(',
    ')',
    '[',
    ']',
    '/',
]

# MONGOURL = 'mongodb://sid:priyankachopra@151.80.41.13:27017/bnp_ent'
# MONGOURL = 'mongodb://51.254.223.26:27017'
MONGOURL = 'mongodb://localhost'
MAX_LENGTH_GRAMS = 5
MIN_LENGTH_GRAMS = 1

# ----
# collections

# collections = ['bnpdb', 'bnpdb2', 'bnpdb3', 'bnpdb4', 'bnpdb5']  # full corpus
# collections = ['bnpdb', 'bnpdb2']  # corpus medium
collections = ['bnpdb', 'bnpdb2']  # corpus medium
# collections = ['bnpdbtest']  # test medium
collections_test = ['bnpdbtest2']  # test small

# ----
# categories

# categories = ['sectors', 'villes_france']
# categories = ['sectors']
categories = ['villes_france']

# ----
# category resources

categories_csv_mapper = {
    'sectors': [0, 1, 1],
    # 'villes_france': [0, 5, 5]
    'villes_france': [0, 1, 1]
}

categories_to_resource_map = {
    'sectors': ['sectors.txt', '\t'],
    # 'villes_france': ['villes_france2.csv', ',']
    'villes_france': ['villes_france_small.csv', ',']
    # 'villes_france': ['villes_france_very_small.csv', ',']
}

# ----
#  stanford resources
#
stanford_rs_classifers = 'F:\\lib\\stanford-ner-2014-06-16\\classifiers\\english.all.3class.distsim.crf.ser.gz'
stanford_rs_ner_jar = 'F:\\lib\\stanford-ner-2014-06-16\\stanford-ner.jar'

# ----
# VERBATIMS EXTRACTION KEYWORD DEFINITION
#
KEYWORD_LIST_FAMILLE = {
    "pere": [
        "mon père", "mon pere", "mon demi père", "mon demi pere", "mon beau père", "mon beau pere"
    ],

    "mere": [
        "ma mère", "ma mere", "ma demi mère", "ma demi mere", "ma belle mère", "ma belle mere"
    ],

    "frere": [
        "mon frère", "mon frere", "mes frères", "mes freres", "mon beau frère", "mon beau frere",
        "mes beaux frères", "mes beau freres", "mon cousin", "mes counsins"
    ],

    "soeur": [
        "ma sœur", "ma soeur", "ma demi sœur", "ma demi soeur", "ma belle sœur", "ma belle soeur", "mes sœur",
        "mes soeurs", "mes belles sœurs", "mes belles soeurs", "ma cousine", "mes cousines"
    ],

    "enfant": [
        "mon enfants", "mes enfants", "mon fils", "mes files", "mes filles", "ma belle filles", "mes belles filles"
        , "mon beau fils", "mes veaux fils", "mon bel enfants", "mes beaux enfants"
    ],

    "parents": [
        "mes parents", "mes beaux parents"
    ],

    "grands_parents": [
        "mes grand parents"
    ],

    "grand_mere": [
        "ma grand mère", "mes grands mères"
    ],

    "grand_pere": [
        "mon grand père", "mes grands pères"
    ],

    "famille": [
        "ma famille", "ma belle famille"
    ],

    "tante": [
        "ma tante", "mes tantes"
    ],

    "oncle": [
        "mon oncle", "mes oncles"
    ],

    "mari": [
        "mon mari", "mes maris", "mon ex mari"
    ],

    "femme": [
        "ma femme", "mes femmes", "mon ex femme"
    ],

    "ami": [
        "mon ami", "mon amie", "mes amis", "mes amies", "mon copain", "ma copine"
        , "mes copains", "mes copaines", "mon pôte", "mon pote", "mes pôtes", "mes potes"
    ]
}

KEYLIST_TOPICS = {
    "accomplissement": [
        "Désir de réussir",
        "j'y arriverai",
        "Persévérer",
        "Détermination ",
        "Je suis déterminé",
        "Je veux réussir",
        "S'investir",
        "Vouloir réussir",
        "Être le meilleur ",
        "Surmonter les problèmes ",
        "Surmonter les situations complexes ",
        "Surmonter les challenges",
        "Faire face aux problèmes",
        "Atteindre ses objectifs",
        "Se fixer des objectifs",
        "Déterminé",
        "Se donner à fond ",
        "Se dépasser",
        "Relever le défi ",
        "Réussite",
        "Persévérant",
        "Cible",
        "But",
        "Succéder",
        "Motivation",
        "Réalisation",
        "courage",
        "envie",
        "Motivé ",
        "Motive",
        "Motivée ",
        "Motivee",
        "Motivation",
    ],

    "autonomie": [
        "Pouvoir de décision",
        "décider",
        "decider",
        "je décide",
        "je decide",
        "Je déciderai",
        "Maître de so",
        "Se gouverner",
        "Indépendance",
        "indépendant",
        "indépendante",
        "Autonome",
        "je me prends en charge",
        "Se prendre en charge",
        "Liberté",
        "Libre",
        "Choisir sois-même",
        "je choisi moi-même",
        "choisis moi-même",
        "prendre les décisions",
        "je prends les décisions",
        "Autonomie",
        "Gérer",
        "Diriger",
        "Faire ses propres choix",
        "Décider pour moi ",
        "Décider pour soi",
        "être le chef",
        "je gère moi-même",
        "Je suis autonome",
        "mes propres choix",
        "Je choisis moi-même",
        "a mon propre compte ",
        "a mon compte",
        "mon propre patron",
    ],

    "innovation": [
        "Originalité",
        "original",
        "originale",
        "Inventivité",
        "inventif",
        "inventive",
        "Innovateur",
        "Innovatrice",
        "innovant",
        "innovante",
        "innovation",
        "Invention",
        "Nouvelles idées",
        "Nouvelle idée",
        "esprit d'ouverture",
        "esprit ouvert",
        "Expérimentale",
        "Expérimental",
        "Concept génération",
        "Curiosité",
        "curieux",
        "curieuse",
        "Imagination",
        "Ouvert au changement",
        "Ouvert aux changements",
        "Disruptif",
        "Disruptive",
        "Nouvelles méthodes",
        "Nouvelle méthode​",
        "Innover",
        "Inventer",
        "j'ai inventé",
        "Nouvelles solutions",
        "Nouvelle solution",
        "j'ai plein d'idée​",
        "J'ai une vision",
        "nouveau produit",
        "nouveaux produits",
        "révolutionnaire​",
        "novateur",
        "novatrice",
        "créatif",
        "créativité",
        "Nouvelles technologies",
        "Nouvelle technologie",
    ],

    "risque": [
        "Incertitude",
        "Incertain",
        "Incertaine",
        "Stabilité",
        "Stable",
        "Sécurité",
        "zone de confort",
        "Craintes",
        "je crains",
        "Doutes",
        "j'ai des doutes",
        "Peur de l'échec",
        "retombées négatives",
        "Appréhensions de l'inconnu",
        "Peur de l'inconnu",
        "Impulsivité",
        "impulsif",
        "impulsive",
        "Spontanéité",
        "spontané",
        "spontanée",
        "Prudence",
        "Oser",
        "je n'ose pas",
        "Casse-cou",
        "Aventurier",
        "Peur de perte",
        "Peur des conséquences",
        "risque",
        "risquee",
        "J'ai peur de l'échec",
        "Je suis prudent",
        "Je suis prudente",
        "Défaite",
        "Insuccès",
        "Je suis spontané",
        "Je suis spontanée",
        "pas sur",
        "pas sûr",
        "j'ai peur",
        "douteux",
        "j'hésite",
        "j'hesite",
        "angoisse",
        "soucieux",
        "effraye",
        "effray",
        "Dangereux",
        "Dangereuse",
        "Danger",
        "Hasard",
        "peur",
    ]
}

KEYWORD_LIST_SENTIMENT = {

    "joyeux": [

        'Alacrité',
        'Allégresse',
        'Amusé',
        'Béat',
        'Charmé',
        'Captivé',
        'Comblé',
        'Content',
        'Détendu',
        'Ébloui',
        'Égayé',
        'Émerveillé',
        'Émoustillé',
        'Enjoué',
        'Enchanté',
        'Enthousiast',
        'Épanoui',
        'Euphorique',
        'Exalté',
        'Excité',
        'Folâtre',
        'Gaieté',
        'Gaillard',
        'Guilleret',
        'Heureux',
        'Hilare',
        'Jovial',
        'Joie',
        'Radieux',
        'Ravi',
        'Réjoui',
        'Remonté',
        'Revigoré',
        'Rieur',
        'Satisfait',
        'Sérénité',
        'Stupéfait',
        'Surexcité',
        'Touché',
        'Vibrant',

    ],

    "tristesse": [

        'Abattement',
        'Abattu',
        'Accablement',
        'Accablé',
        'Affliction',
        'Affligé',
        'Anéantissement',
        'Anéanti',
        'Atterrement',
        'Atterré',
        'Bouleversement',
        'Bouleversé',
        'Chagrin',
        'Chagriné',
        'Consternation',
        'Consterné',
        'Déchirement',
        'Déchiré',
        'Désabusement',
        'Désabusé',
        'Désenchantement',
        'Désenchanté',
        'Désespoir',
        'Désespéré',
        'Désolation',
        'Désolé',
        'Eplorement',
        'Éploré',
        'Malheur',
        'Malheureux',
        'Maussaderie',
        'Maussade',
        'Mélancolie',
        'Mélancolique',
        'Morosité',
        'Morose',
        'Nostalgie',
        'Nostalgique',
        'Peine',
        'Peiné',
        'Souci',
        'Soucieux',
        'Taciturnité',
        'Taciturne',

    ],

    "colere": [

        'Agacé',
        'Contrarié',
        'Crispé',
        'De mauvaise humeur',
        'Courroucé',
        'Enragé',
        'Ecœuré',
        'En colère',
        'Énervé',
        'Enragé',
        'Exaspéré',
        'Excédé',
        'Fâché',
        'Frustré',
        'Furieux',
        'Haineux',
        'Irrité',
        'Mécontent',
        'Nerveux',
        'Remonté',

    ],

    "degout": [

        'Amertume',
        'Amer',
        'Désabusé',
        'Désenchantement',
        'Désenchanté',
        'Désillusion',
        'Désillusionné',
        'Ecoeurement',
        'Écœuré',
        'Horripilé',
        'Incommodé',
        'Ulcéré',

    ]

}
