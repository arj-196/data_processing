import counter as dp
import resources as rs
import mapper as mp
from pymongo import MongoClient
import re
from sets import Set
from datetime import datetime
import sys, traceback
import os
from debugger.debugger import Debugger

startTime = datetime.now()
from sets import Set


class UniqueNGramsCounter(object):
    clean_cache_interval = 20000
    iteration = 0
    db_grams = None  # database to store gram frequencies
    map = None  # hash index to frequency
    cache = None  # temporary hash index to gram
    gram_length_list = None  # hash index to gram length
    debug = ""

    def __init__(self, db_gram, clean_cache=True):
        self.db_grams = db_gram  # defining dbase
        self.map = {}
        self.cache = {}
        self.gram_length_list = {}
        self.if_clean_cache = clean_cache
        for l in range(rs.MIN_LENGTH_GRAMS, rs.MAX_LENGTH_GRAMS + 1):
            self.gram_length_list[l] = Set([])

    def __del__(self):
        print "UNIQUE GRAM COUNTER : stopped at ", self.debug
        self.clean_cache()
        self.remove_inrelevant()

    @staticmethod
    def clean_sentence(sentence):
        # to lower
        s = sentence.lower()
        # remove all puntutations
        for p in rs.puntuations:
            s = s.replace(p, " ")
        # remove multiple spaces
        s = re.sub('\d', '', s)
        s = re.sub(' +', ' ', s)
        return s

    def run(self, collections, _type="mongo"):
        print "UNIQUE NGRAM COUNTER : Start run"

        if _type == "mongo":
            db = client.bnp_ent
            for collection in collections:
                print "-- collection ", collection
                for thread in db[collection].find({"posts": {"$exists": "true", "$not": {"$size": 0}}},
                                                  {"_id": 0, "posts": 1}):
                    if 'posts' in thread:
                        for post in thread['posts']:
                            self.debug = collection + " " + str(self.iteration)
                            self.iteration += 1
                            if 'content' in post:
                                content = self.clean_sentence(post['content'])
                                tokens = content.split(' ')
                                self._get_grams(tokens)

                # clean cache at interval
                if self.iteration % self.clean_cache_interval == 0:
                    self.clean_cache()

        if _type == "file":
            SEP = '\t'
            NEWLINE = '\n'
            for collection in collections:
                print "-- collection", collection.name
                lines = collection.read()
                for row in lines.split(NEWLINE):
                    self.iteration += 1
                    self.debug = collection.name + " " + str(self.iteration)
                    try:
                        content = self.clean_sentence(row.decode('utf-8'))
                        tokens = content.split(' ')
                        self._get_grams(tokens)
                    except:
                        e = sys.exc_info()[0]
                        print ("ERROR  %s", e)
                        traceback.print_exc(file=sys.stdout)

                # clean cache at interval
                if self.iteration % self.clean_cache_interval == 0:
                    self.clean_cache()

                    #
                    # test break condition
                    # if self.iteration > 5:
                    #     print "TEST: PREMATURE BREAK"
                    #     exit(196)
        print "UNIQUE NGRAM COUNTER : End run", datetime.now() - startTime

    def _get_grams(self, tokens):
        ngram_set = dp.Utils.get_ngrams_set(tokens)
        for grams in ngram_set:
            for gram in grams:
                self._analyze_gram(gram)

    def _analyze_gram(self, gram):
        id_gram = mp.NGramMapper.hash_ngram(gram)
        length = len(gram)

        # update map
        if id_gram not in self.map:
            self.map[id_gram] = 1
        else:
            self.map[id_gram] += 1

        # update gram_length_list
        if id_gram not in self.gram_length_list[length]:
            self.gram_length_list[length].add(id_gram)

        # update cache
        if id_gram not in self.cache:
            self.cache[id_gram] = gram

    def clean_cache(self):
        if self.if_clean_cache:
            print "UNIQUE NGRAM COUNTER : Clear Cache", datetime.now() - startTime
            for length in self.gram_length_list.keys():
                col = self.db_grams["length_" + str(length)]
                for id_gram in self.gram_length_list[length]:
                    if self.map[id_gram] > threshold[length]:
                        # check in db
                        item = col.find_one({"_id": id_gram})
                        if not item:
                            col.insert({
                                "_id": id_gram,
                                "frequency": self.map[id_gram],
                                "gram": self.cache[id_gram]
                            })
                        else:
                            new_freq = self.map[id_gram] + int(item['frequency'])
                            col.update_one(
                                {"_id": id_gram},
                                {
                                    "$set": {
                                        "frequency": new_freq
                                    }
                                }
                            )

        # clear cache and gram_length_list
        self.cache = {}
        self.gram_length_list = {}
        for l in range(rs.MIN_LENGTH_GRAMS, rs.MAX_LENGTH_GRAMS + 1):
            self.gram_length_list[l] = Set([])

    def remove_inrelevant(self):
        print "UNIQUE NGRAM COUNTER : Delete Unrelevant", datetime.now() - startTime
        for l in range(rs.MIN_LENGTH_GRAMS, rs.MAX_LENGTH_GRAMS + 1):
            print "length_" + str(l), {"frequency": {"$lt": str(threshold[l])}}, datetime.now() - startTime
            self.db_grams["length_" + str(l)].remove({"frequency": {"$lt": threshold[l]}})

    def export_results(self, filename):
        file_suffix = '.csv'
        sep = '\t'
        newline = '\n'
        print "UNIQUE NGRAM COUNTER : Export"
        for l in range(rs.MIN_LENGTH_GRAMS, rs.MAX_LENGTH_GRAMS + 1):
            print "length_" + str(l)
            col_name = "length_" + str(l)
            f = open(filename + col_name + file_suffix, 'w')
            f.close()
            f = open(filename + col_name + file_suffix, 'a')
            for o in self.db_grams[col_name].find({"gram": {"$size": l}}):
                string = ""
                string += " ".join(o['gram']) + sep
                string += str(o['frequency']) + newline
                f.write(string.encode("utf8", "ignore"))
            f.close()

    def export_results_by_category(self, categories, filename):
        file_suffix = '.csv'
        sep = '\t'
        newline = '\n'
        print "UNIQUE NGRAM COUNTER : Export"
        for category in categories.keys():
            print "--CATEGORY", category
            visited_ids = []
            _list = categories[category]
            for l in range(rs.MIN_LENGTH_GRAMS, rs.MAX_LENGTH_GRAMS + 1):
                print "length_" + str(l)
                col_name = "length_" + str(l)
                f = open(filename + category + "_" + col_name + file_suffix, 'w')
                f.close()
                f = open(filename + category + "_" + col_name + file_suffix, 'a')
                for e in _list:
                    tokens = self.clean_sentence(e).encode('utf-8').split(' ')  # split on whitespace
                    for o in self.db_grams[col_name].find(
                            {"gram": {"$size": l, "$all": tokens}, "_id": {"$nin": visited_ids}}):
                        visited_ids.append(o["_id"])
                        string = ""
                        string += " ".join(o['gram']) + sep
                        string += str(o['frequency']) + newline
                        f.write(string.encode("utf8", "ignore"))
                f.close()


# ---
# define threshold
#
threshold = {
    1: 0,
    2: 0,
    3: 0,
    4: 0,
    5: 0,
    6: 0
}
# threshold = {
#     1: 5000,
#     2: 3500,
#     3: 2000,
#     4: 2000,
#     5: 500
# }
client = MongoClient('mongodb://localhost:27018')
db_grams = client.keywords_clusters_c  # choosing db


def custom():
    # cluster 0
    counter = UniqueNGramsCounter(client.grams_clusters_c_0, clean_cache=True)  # choosing db
    ### tsv files
    _dir = "verbatims/backup/c/0/"
    files = []
    for _file in os.listdir(_dir):
        if os.path.isfile(_dir + _file):
            files.append(open(_dir + _file, 'r'))
    counter.run(files, _type="file")
    counter.clean_cache()
    # counter.export_results_by_category(rs.KEYLIST_TOPICS, "results/unique_grams_categories_2_cluster_0_")

    # # cluster 1
    # counter = UniqueNGramsCounter(client.grams_clusters_c_1, clean_cache=True)  # choosing db
    # ### tsv files
    # # _dir = "verbatims/backup/c/1/"
    # # files = []
    # # for _file in os.listdir(_dir):
    # #     if os.path.isfile(_dir + _file):
    # #         files.append(open(_dir + _file, 'r'))
    # # counter.run(files, _type="file")
    # # counter.clean_cache()
    # counter.export_results_by_category(rs.KEYLIST_TOPICS, "results/unique_grams_categories_2_cluster_1_")

    # # cluster 2
    # counter = UniqueNGramsCounter(client.grams_clusters_c_2, clean_cache=True)  # choosing db
    # ### tsv files
    # # _dir = "verbatims/backup/c/2/"
    # # files = []
    # # for _file in os.listdir(_dir):
    # #     if os.path.isfile(_dir + _file):
    # #         files.append(open(_dir + _file, 'r'))
    # # counter.run(files, _type="file")
    # # counter.clean_cache()
    # counter.export_results_by_category(rs.KEYLIST_TOPICS, "results/unique_grams_categories_2_cluster_2_")

    # # cluster 3
    # counter = UniqueNGramsCounter(client.grams_clusters_c_3, clean_cache=True)  # choosing db
    # ### tsv files
    # # _dir = "verbatims/backup/c/3/"
    # # files = []
    # # for _file in os.listdir(_dir):
    # #     if os.path.isfile(_dir + _file):
    # #         files.append(open(_dir + _file, 'r'))
    # # counter.run(files, _type="file")
    # # counter.clean_cache()
    # counter.export_results_by_category(rs.KEYLIST_TOPICS, "results/unique_grams_categories_2_cluster_3_")

    # # cluster 4
    # counter = UniqueNGramsCounter(client.grams_clusters_c_4, clean_cache=True)  # choosing db
    # ### tsv files
    # # _dir = "verbatims/backup/c/4/"
    # # files = []
    # # for _file in os.listdir(_dir):
    # #     if os.path.isfile(_dir + _file):
    # #         files.append(open(_dir + _file, 'r'))
    # # counter.run(files, _type="file")
    # # counter.clean_cache()
    # counter.export_results_by_category(rs.KEYLIST_TOPICS, "results/unique_grams_categories_2_cluster_4_")


def main():
    counter = UniqueNGramsCounter(db_grams)

    ### mongo
    # counter.run(['bnpdb2', 'bnpdb3', 'bnpdb4', 'bnpdb5'], type="mongo")

    ### tsv files
    _dir = "verbatims/backup/c/"
    files = []
    for _file in os.listdir(_dir):
        if os.path.isfile(_dir + _file):
            files.append(open(_dir + _file, 'r'))

    counter.run(files, _type="file")


def export(filename):
    counter = UniqueNGramsCounter(db_grams, clean_cache=False)
    counter.export_results("results/unique_grams_" + filename + '_')


def export_categories(filename):
    counter = UniqueNGramsCounter(db_grams, clean_cache=False)
    counter.export_results_by_category(rs.KEYLIST_TOPICS, "results/unique_grams_categories_" + filename + "_")


if __name__ == '__main__':
    argv = sys.argv
    if argv[1] == 'calculate':
        main()
    if argv[1] == 'custom':
        custom()
    if argv[1] == 'export':
        export(argv[2])

    if argv[1] == 'exportcategories':
        export_categories(argv[2])

#
# python unique_grams calculate
# python unique_grams export _filename
# python unique_grams exportcategories _filename
#
