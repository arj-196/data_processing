import mapper as mp
import resources as rs
import counter as dp
import sys


def noun_counter():
    print "--- NOUN"
    mappers = mp.MapperCollection().create_mappers(mp.NGramMapper, rs.categories, rs.categories_csv_mapper,
                                                   rs.categories_to_resource_map)
    analyzers = dp.UniTextFRCollection().create_analyzers(rs.collections_test)

    counter = dp.NounCounter()
    counter.init_variables(analyzers, mappers)
    counter.run()


# advances category counter
def ngram_counter():
    print "--- NGRAM"
    mappers = mp.MapperCollection().create_mappers(mp.NGramMapper, rs.categories, rs.categories_csv_mapper,
                                                   rs.categories_to_resource_map)
    analyzers = dp.UniTextFRCollection().create_analyzers(['bnpdb'])
    counter = dp.NGramCounter()
    counter.init_variables(analyzers, mappers)
    counter.run("ad_1_")

    del analyzers; del mappers; del counter
    mappers = mp.MapperCollection().create_mappers(mp.NGramMapper, rs.categories, rs.categories_csv_mapper,
                                                   rs.categories_to_resource_map)
    analyzers = dp.UniTextFRCollection().create_analyzers(['bnpdb2'])
    counter = dp.NGramCounter()
    counter.init_variables(analyzers, mappers)
    counter.run("ad_2_")

    del analyzers; del mappers; del counter
    mappers = mp.MapperCollection().create_mappers(mp.NGramMapper, rs.categories, rs.categories_csv_mapper,
                                                   rs.categories_to_resource_map)
    analyzers = dp.UniTextFRCollection().create_analyzers(['bnpdb3'])
    counter = dp.NGramCounter()
    counter.init_variables(analyzers, mappers)
    counter.run("ad_3_")

    del analyzers; del mappers; del counter
    mappers = mp.MapperCollection().create_mappers(mp.NGramMapper, rs.categories, rs.categories_csv_mapper,
                                                   rs.categories_to_resource_map)
    analyzers = dp.UniTextFRCollection().create_analyzers(['bnpdb4'])
    counter = dp.NGramCounter()
    counter.init_variables(analyzers, mappers)
    counter.run("ad_4_")

    del analyzers; del mappers; del counter
    mappers = mp.MapperCollection().create_mappers(mp.NGramMapper, rs.categories, rs.categories_csv_mapper,
                                                   rs.categories_to_resource_map)
    analyzers = dp.UniTextFRCollection().create_analyzers(['bnpdb5'])
    counter = dp.NGramCounter()
    counter.init_variables(analyzers, mappers)
    counter.run("ad_5_")


# entity recoginition counter
def ner_counter():
    print "--- NER"
    mappers = []

    analyzers = dp.UniTextFRCollection().create_analyzers(['bnpdb2'])
    analyzers[0]['analyzer'].extract_pos()
    counter = dp.NERCounter()
    counter.init_variables(analyzers, mappers)
    counter.run("2")
    counter.export_results("2")

    analyzers = dp.UniTextFRCollection().create_analyzers(['bnpdb3'])
    analyzers[0]['analyzer'].extract_pos()
    counter = dp.NERCounter()
    counter.init_variables(analyzers, mappers)
    counter.run("3")
    counter.export_results("3")

    analyzers = dp.UniTextFRCollection().create_analyzers(['bnpdb4'])
    analyzers[0]['analyzer'].extract_pos()
    counter = dp.NERCounter()
    counter.init_variables(analyzers, mappers)
    counter.run("4")
    counter.export_results("4")

    analyzers = dp.UniTextFRCollection().create_analyzers(['bnpdb5'])
    analyzers[0]['analyzer'].extract_pos()
    counter = dp.NERCounter()
    counter.init_variables(analyzers, mappers)
    counter.run("5")
    counter.export_results("5")


def location_counter():
    mappers = mp.MapperCollection().create_mappers(mp.StringMapper, rs.categories, rs.categories_csv_mapper,
                                                   rs.categories_to_resource_map)
    analyzers = dp.UniTextFRCollection().create_analyzers(['bnpdb'])
    counter = dp.LocationCounter()
    counter.init_variables(analyzers, mappers)
    counter.run("small_cities_1")


def word_counter():
    mappers = []
    # analyzers = dp.UniTextFRCollection().create_analyzers(['bnpdb'])
    # analyzers[0]['analyzer'].extract_pos()
    # counter = dp.WordCounter()
    # counter.init_variables(analyzers, mappers)
    # counter.run("var_1_")
    # counter.export_results("1")

    # analyzers = dp.UniTextFRCollection().create_analyzers(['bnpdb2'])
    # analyzers[0]['analyzer'].extract_pos()
    # counter = dp.WordCounter()
    # counter.init_variables(analyzers, mappers)
    # counter.run("var_2_")
    # counter.export_results("2")

    # analyzers = dp.UniTextFRCollection().create_analyzers(['bnpdb3'])
    # analyzers[0]['analyzer'].extract_pos()
    # counter = dp.WordCounter()
    # counter.init_variables(analyzers, mappers)
    # counter.run("var_3_")
    # counter.export_results("3")

    analyzers = dp.UniTextFRCollection().create_analyzers(['bnpdb4'])
    analyzers[0]['analyzer'].extract_pos()
    counter = dp.WordCounter()
    counter.init_variables(analyzers, mappers)
    counter.run("var_4_")
    counter.export_results("4")

    analyzers = dp.UniTextFRCollection().create_analyzers(['bnpdb5'])
    analyzers[0]['analyzer'].extract_pos()
    counter = dp.WordCounter()
    counter.init_variables(analyzers, mappers)
    counter.run("var_5_")
    counter.export_results("5")


def unique_ngrams():
    counter = dp.WordCounter()
    counter.init_variables(analyzers, mappers)
    counter.run("var_4_")
    counter.export_results("4")



if __name__ == '__main__':
    if len(sys.argv) == 1:
        print "ERROR: NOT ENOUGH PARAMETERS"
    else:
        if sys.argv[1] == 'ad':
            ngram_counter()
        if sys.argv[1] == 'ner':
            ner_counter()
        if sys.argv[1] == 'loc':
            location_counter()
        if sys.argv[1] == 'word':
            word_counter()
        if sys.argv[1] == 'ngram':
            ngram_counter()


