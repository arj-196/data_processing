# -*- coding: utf-8 -*-
from datetime import datetime

startTime = datetime.now()

# import pandas as pd
# from pymongo import MongoClient
# from nltk import word_tokenize
# from pattern.text.fr import tokenize
# import pickle
# from mapper import StringMapper
# import csv
# from data_process import CategoryCounter, Writer
# s = "je veux bien  d'etre la mais "
# print "whitespace", s.split()
# print "nltk", word_tokenize(s)
# print "pattern", tokenize(s)


# import nltk
# from nltk.corpus import state_union
# from nltk.tokenize import PunktSentenceTokenizer

"""
    Alchemy API
    key: 0ebeec5ac9fbbdeb6ee93ad5ddb501f593bf5847
"""
import json
# from alchemyapi import AlchemyAPI
# alchemyapi = AlchemyAPI()

# myText = "I'm excited to get started with AlchemyAPI!"
# response = alchemyapi.sentiment("text", myText)
# print "Sentiment: ", response["docSentiment"]["type"]


# text = [
#     """
#     Modeling[edit]
#     Monica Bellucci began modeling at age 13 by posing for a local photo enthusiast.[4] In 1988, Bellucci moved to one of Europe's fashion centres, Milan, where she signed with Elite Model Management. By 1989, she was becoming prominent as a fashion model in Paris and across the Atlantic, in New York City. She posed for Dolce & Gabbana and French Elle, among others. In that year, Bellucci made the transition to acting and began taking acting classes. The February 2001 Esquire's feature on Desire featured Bellucci on the cover and in an article on the five senses. In 2003, she was featured in Maxim.[5] Men's Health also named her one of the "100 Hottest Women of All-Time", ranking her at No. 21.[6] AskMen named her the number one most desirable woman in 2002.[7] According to two national newspapers, she is considered an Italian sex symbol.[8][9] In 2004, while pregnant with her daughter Deva, Bellucci posed nude for the Italian Vanity Fair in protest against Italian laws that prevent the use of donor sperm.[10] She posed pregnant and semi-nude again for the magazine's April 2010 issue.[11] From 2006 to 2010 Bellucci was the face of a range of Dior products.[12] In 2012, she became the new face of Dolce & Gabbana.[13] She is managed by Elite+ in New York City. Bellucci is signed to D'management group in Milan and also to Storm Model Management in London.[14]
#
#     Acting[edit]
#
#     Monica Bellucci at Cannes Film Festival in 2002.
#     Bellucci's film career began in the early 1990s. She played some minor roles in La Riffa (1991) and Bram Stoker's Dracula (1992). In 1996 she was nominated for a César Award for best supporting actress for her portrayal of Lisa in The Apartment[15] and strengthened her position as an actress. She became known and popular with worldwide audiences, following her roles in Malèna (2000), Brotherhood of the Wolf, "Under Suspicion",[16] and Irréversible (2002). She has since played in many films from Europe and Hollywood like Tears of the Sun (2003), The Matrix Reloaded (2003), The Passion of the Christ (2004), The Brothers Grimm (2005), Le Deuxième souffle (2007), Shoot 'Em Up (2007), Don't Look Back (2009), and The Sorcerer's Apprentice (2010).[17][18]
#
#     She was supposed to be seen portraying Indian politician Sonia Gandhi in the biopic Sonia, originally planned for release in 2007, but it has been shelved.[19] Bellucci dubbed her own voice for the French and Italian releases of the film Shoot 'Em Up (2007).[20] She also voiced Kaileena in the video game Prince of Persia: Warrior Within, and the French voice of Cappy for the French version of the 2005 animated film Robots.
#
#     Empire magazine selected her as twenty-first on their list of "The Sexiest 25 Characters in Cinema – The Women" for her role of "Persephone" in the Matrix series.[21] Likewise, Nettavisen declared in 2009 that the role of Persephone qualified Bellucci as one of the 25 sexiest women of all time.[22]
#
#     At 50, she will be the oldest Bond girl ever in the James Bond film franchise, playing Lucia Sciarra in Spectre (2015).[23]
#     """,
#
#     """
#     Modélisation [modifier]
#     Monica Bellucci a commencé le mannequinat à 13 ans en se faisant passer pour un passionné de photo local. [4] En 1988, Monica Bellucci a déménagé à l'un des centres de la mode en Europe, Milan, où elle a signé avec Elite Model Management de. En 1989, elle a été de plus en plus important en tant que modèle de mode à Paris et à travers l'Atlantique, à New York. Elle a posé pour Dolce & Gabbana et Elle français, entre autres. En cette année, Bellucci a fait la transition à agir et a commencé à prendre des cours de théâtre. La fonction de la Février 2001 Esquire sur le désir sélectionnée Bellucci sur la couverture et dans un article sur les cinq sens. En 2003, elle a été présentée dans Maxim. [5] la santé des hommes aussi nommé son l'un des «100 femmes les plus chaudes de tous les temps", son classement au n ° 21. [6] AskMen a nommée le nombre une femme plus désirable 2002. [7] Selon deux journaux nationaux, elle est considérée comme un sex-symbol italien. [8] [9] En 2004, alors enceinte de sa fille Deva, Monica Bellucci a posé nue pour le magazine Vanity Fair italien en signe de protestation contre les lois italiennes qui empêchent l'utilisation de sperme de donneur. [10] Elle a posé enceinte et semi-nue à nouveau pour Avril 2010 numéro de la revue. [11] De 2006 à 2010 Bellucci était le visage d'une gamme de produits Dior. [12] En 2012, elle est devenue le nouveau visage de Dolce & Gabbana. [13] Elle est gérée par Elite + à New York. Bellucci est signé au groupe de gestion D'à Milan et également Tempête Model Management à Londres. [14]
#
#     Agissant [modifier]
#
#     Monica Bellucci au Festival de Cannes en 2002.
#     La carrière cinématographique de Bellucci a commencé dans les années 1990. Elle a joué quelques rôles mineurs dans La Riffa (1991) et Dracula de Bram Stoker (1992). En 1996, elle a été en nomination pour un César de la meilleure actrice de soutien pour son interprétation de Lisa dans l'appartement [15] et de renforcer sa position comme une actrice. Elle est devenue connue et populaire auprès du public à travers le monde, suite à ses rôles dans Malèna (2000), Le Pacte des loups, "Under Suspicion", [16] et Irréversible (2002). Depuis, elle a joué dans de nombreux films en provenance d'Europe et Hollywood comme Larmes du soleil (2003), Matrix Reloaded (2003), La Passion du Christ (2004), Les Frères Grimm (2005), Le Deuxième souffle (2007), Shoot 'Em Up (2007), Do not Look Back (2009), et L'Apprenti sorcier (2010). [17] [18]
#
#     Elle était censée être vu dépeignant politique indienne Sonia Gandhi dans le biopic Sonia, initialement prévu pour une sortie en 2007, mais il a été mis en veilleuse. [19] Bellucci doublé sa propre voix pour les versions françaises et italiennes du film Shoot 'Em Up . (2007) [20] Elle a également exprimé Kaileena dans le jeu vidéo Prince of Persia: Warrior Within, et la voix française de Cappy pour la version française du film d'animation Robots de 2005.
#
#     Magazine Empire l'a choisie comme vingt et unième sur leur liste de. "Le plus sexy de 25 caractères dans Cinéma - Les Femmes" pour son rôle de "Perséphone" dans la série Matrix [21] De même, Nettavisen déclaré en 2009 que le rôle de Perséphone qualifiée Bellucci comme l'une des 25 femmes les plus sexy de tous les temps. [22]
#
#     À 50 ans, elle sera la plus ancienne James Bond girl jamais dans la franchise de film de James Bond, en jouant Lucia Sciarra Spectre (2015). [23]
#     """]

#
# entity recognition
#

# for i, t in enumerate(text):
#     response = alchemyapi.entities('text', t, {'sentiment': 1})
#
#     if response['status'] == 'OK':
#         print('## Response Object ##')
#         print(json.dumps(response, indent=4))
#         with open('results/entity_recognition_' + str(i) + '.json', 'w') as outfile:
#             json.dump(response, outfile)
#     else:
#         print('Error in entity extraction call: ', response['statusInfo'])


#
# Stanford NER
#

# from nltk.tag.stanford import StanfordNERTagger
#
# st = StanfordNERTagger('F:\\lib\\stanford-ner-2014-06-16\\classifiers\\english.all.3class.distsim.crf.ser.gz',
#                        'F:\\lib\\stanford-ner-2014-06-16\\stanford-ner.jar')
#
# for i, t in enumerate(text):
#     tokens = t.split()
#     encoded_tokens = []
#     # take only encoded tokens
#     for token in tokens:
#         try:
#             encoded_tokens.append(token.encode('utf8'))
#         except:
#             pass
#     tagged = st.tag(encoded_tokens)
#     response = {}
#     for tag in tagged:
#         if tag[1] in response:
#             if tag[0] not in response[tag[1]]:
#                 response[tag[1]].append(tag[0])
#         else:
#             response[tag[1]] = [tag[0]]
#
#     with open('results/entity_recognition_stanford_' + str(i) + '.json', 'w') as outfile:
#         json.dump(response, outfile)


#
# Time requirements
#
# from pymongo import MongoClient
# import re
# import mapper as mp
# import resources as rs
# import counter as dp
#
#
# db = MongoClient("localhost").bnp_ent
# col = db.bnpdb
#
# mapper = mp.MapperCollection().create_mappers(mp.StringMapper, rs.categories, rs.categories_csv_mapper,
#                                               rs.categories_to_resource_map)[0]['mapper']
# count = 0
# iteration = 0
# RANGE = 10
# for o in col.find({"posts": {"$exists": "true", "$not": {"$size": 0}}}, {"_id": 0, "posts": 1})[0:RANGE]:
#     if 'posts' in o:
#         count += len(o['posts'])
#
#         for post in o['posts']:
#             if 'content' in post:
#                 content = post['content']
#                 for pattern in mapper.map:
#                     ptn = re.compile('.*' + pattern + '.*')
#                     r = re.search(ptn, content)
#
#                     #
#                     iteration += 1
#
#                     if iteration % 1000 == 0:
#                         print "iteration", iteration
#
#
# #
# #
# print "\n#\n#\n#\n#"
# print "total iterations", iteration
# print "range 0 :", RANGE
# print "mapper list length", len(mapper.map)
# print "content length", count
# print "average post length", count / RANGE
# print "total collection size", col.count({"posts": {"$exists": "true", "$not": {"$size": 0}}})

#
# ngrams
#

from pattern.text.fr import ngrams
import counter as dp

text = "hello world! how are you?"

ngram_set = dp.Utils.get_ngrams_set(text.split(' '))

print ngram_set

#
#
print "end", datetime.now() - startTime
