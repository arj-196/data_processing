# -*- coding: utf-8 -*-
import resources as rs
from pymongo import MongoClient
import re
from datetime import datetime
import csv
import sys, traceback
import atexit
import pickle

startTime = datetime.now()

SEP = '\t'
NEWLINE = '\n'

def before_exit():
    print "EXTRACTER EXIT", datetime.now() - startTime

def create_keyword_files(_keyword_list):
    files = {}
    for keyword in _keyword_list.keys():
        if keyword not in files:
            files[keyword] = open('results/' + keyword + '.tsv', 'a')

    return files


def clean_sentence(sentence):
    # to lower
    s = sentence.lower()
    # remove all puntutations
    for p in rs.puntuations:
        s = s.replace(p, " ")
    # remove multiple spaces
    s = re.sub('\d', '', s)
    s = re.sub(' +', ' ', s)
    return s


class Extracter(object):

    def __init__(self):
        # at exist handler
        atexit.register(before_exit)


    @staticmethod
    def start_extract(_collections, _keyword_list):
        print "EXTRACTER Start", datetime.now() - startTime

        keyword_files = create_keyword_files(_keyword_list)
        count = 0
        for col in _collections:
            for row in col.iterrows():
                count += 1

                # debug print
                if count % 10000 == 0:
                    print "EXTRACTER Debug : Iteration", count, datetime.now() - startTime

                try:
                    doc = row[1]  #
                    post = doc.Corpus

                    for category in _keyword_list.keys():
                        _list = _keyword_list[category]
                        for keyword in _list:
                            try:
                                keyword = clean_sentence(keyword.decode("utf-8"))
                                sentence = clean_sentence(post)

                                if keyword in sentence:
                                    try:
                                        string = post.encode("utf-8")
                                        string = string.replace('\n', ' ')
                                        string = string.replace('\t', ' ')
                                        re.sub(' +', ' ', string)
                                        string += NEWLINE
                                        keyword_files[category].write(string)
                                        count += 1
                                    except:
                                        print "Iteration", count
                                        e = sys.exc_info()[0]
                                        print ("ERROR  %s", e)
                                        traceback.print_exc(file=sys.stdout)

                                        # print "Iteration", count, doc["_id"], len(doc["posts"]), category, keyword
                            except:
                                print "Iteration", count
                                e = sys.exc_info()[0]
                                print ("ERROR  %s", e)
                                traceback.print_exc(file=sys.stdout)
                except:
                    print "Iteration", count
                    e = sys.exc_info()[0]
                    print ("ERROR %s", e)
                    traceback.print_exc(file=sys.stdout)


if __name__ == '__main__':

    #### define collections

    # client = MongoClient('mongodb://sid:priyankachopra@151.80.41.13:27017/bnp_ent')
    client = MongoClient('localhost')
    db = client.bnp_ent
    # collections = [db.bnpdb5, db.bnpdb, db.bnpdb2, db.bnpdb3, db.bnpdb4]
    # collections = [db.bnpdb5, db.bnpdb, db.bnpdb2, db.bnpdb3, db.bnpdb4]
    collections = [pickle.load(open("data/test_set.pickle", "r"))]

    #### define keywords
    keyword_list = rs.KEYWORD_LIST_FAMILLE

    #### run extracter
    Extracter().start_extract(collections, keyword_list)
