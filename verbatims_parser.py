import sys, traceback
import os
import csv
from datetime import datetime
import re
import json
import hashlib as hl
from sets import Set
import pandas as pd
startTime = datetime.now()


def _log(_msg, _type):
    if _type == 1:
        print "log:", _msg, datetime.now() - startTime


def _read_files(_dir, _file):
    if not os.path.exists(_dir):
        raise Exception("dir does not exist: " + _dir, 1)
    files = []
    if not _file == '$':
        if not os.path.exists(_dir + _file):
            raise Exception("file does not exist: " + _dir + _file, 1)
        else:
            files.append(open(_dir + _file, 'r'))
    else:
        for f in os.listdir(_dir):
            files.append(open(_dir + f, 'r'))

    return files


def _extract_verbatims(f):
    verbatims = []
    delimeter = "VERBATIM"
    start = True
    index = 0

    content = f.read()
    while index != -1:
        index = content.find(delimeter)
        if index > -1:
            if start:
                start = False
            else:
                if index > 0:
                    text = content[0:index - 1].decode('utf8', 'ignore')
                    pattern_score = re.compile('\(score:\s?[-+]?([0-9]*\.[0-9]+|[0-9]+)\)')
                    r = re.findall(pattern_score, text)
                    if r:
                        score = r[0]
                    else:
                        score = 0

                    text = re.sub(pattern_score, "", text)
                    verbatims.append([text, score])
            #
            content = content[index + len(delimeter):len(content)]
    return verbatims

def _extract_verbatims_panda_csv(f):
    verbatims = []
    content = pd.read_csv(f.name)
    for i, row in content.iterrows():
        v = []
        for d in row:
            if type(d) is str or type(d) is unicode:
                s = "\"" + d + "\""
                # replace newlines
                s = s.replace('\n', ' ')
                s = s.decode('utf8', 'ignore')
                v.append(s)
            else:
                v.append(str(d))

        verbatims.append([",".join(v), 0])
    return verbatims

def _remove_duplicates(verbatims):
    unique_verbatims = []
    verbatim_hash_keys = Set([])
    for verbatim in verbatims:
        hash_key = hl.md5("".join(verbatim[0]).encode("utf-8")).hexdigest()
        if hash_key not in verbatim_hash_keys:
            unique_verbatims.append(verbatim)
            verbatim_hash_keys.add(hash_key)

    return unique_verbatims


def _write_as_tsv(verbatims, filename):
    verbatims = _remove_duplicates(verbatims)
    sep = '\t'
    newline = '\n'
    # f = open('verbatims/' + filename, 'a')
    with open('verbatims/' + filename, 'wb') as tsvfile:
        spamwriter = csv.writer(tsvfile, delimiter='\t')
        for item in verbatims:
            try:
                string = item[0].encode('utf8', 'ignore')
                string = string.replace('\n', ' ')
                string = string.replace('\t', ' ')
                re.sub(' +', ' ', string)
                spamwriter.writerow([string, item[1]])
            except:
                e = sys.exc_info()[0]
                print ("ERROR %s", e)
                traceback.print_exc(file=sys.stdout)

    # for item in verbatims:
    #     try:
    #         # string = item[0].encode('utf8', 'ignore')
    #         # string = string.replace('\n', ' ')
    #         # string = string.replace('\t', ' ')
    #         # re.sub(' +', ' ', string)
    #         # f.write(string)
    #     except:
    #         e = sys.exc_info()[0]
    #         print ("ERROR %s", e)
    #         traceback.print_exc(file=sys.stdout)


def parse(_dir, _file):
    print "VERBATIMS PARSER : Start "
    files = _read_files(_dir, _file)
    for f in files:
        verbatims = _extract_verbatims(f)
        filename = f.name.split('/')
        _write_as_tsv(verbatims, filename[len(filename) - 1])
        f.close()
    print "VERBATIMS PARSER : End "


def parse_pandacsv(_dir, _file, columnnames):
    print "VERBATIMS PARSER PANDACSV : Start "
    files = _read_files(_dir, _file)
    for f in files:
        verbatims = _extract_verbatims_panda_csv(f)
        filename = f.name.split('/')
        _write_as_tsv(verbatims, filename[len(filename) - 1])
        f.close()
    print "VERBATIMS PARSER PANDACSV : End "



if __name__ == '__main__':
    print sys.argv

    if sys.argv[2:]:
        if len(sys.argv) == 3:
            parse(sys.argv[1], sys.argv[2])
        elif len(sys.argv) == 5:
            if sys.argv[1] == "pandacsv":
                parse_pandacsv(sys.argv[2], sys.argv[3], sys.argv[4])
            else:
                print "Command Not Understood"
        else:
            print "Command Not Understood"
    else:
        print "Not all parameters specified"

#
# python verbatim_parser.py data/banks/ $
# python verbatim_parser.py pandacsv data/creditmutuel/ $ index,verbatim
