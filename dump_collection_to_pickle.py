from pymongo import MongoClient
import sys, traceback
import pickle


def create_pickle(filename):
    print "DUMP TO PICKLE : Start"
    for col in collections:
        dump_file = open(filename + '_' + col + '.p', 'wb')
        contents = []
        for thread in db[col].find({"posts": {"$exists": "true", "$not": {"$size": 0}}}, {"_id": 0, "posts": 1}):
            contents.append(thread)

        pickle.dump(contents, dump_file)
    print "DUMP TO PICKLE : End"


def read_pickle(filename):
    contents = pickle.load(open(filename, 'rb'))
    print ""


if __name__ == '__main__':
    argv = sys.argv
    if len(argv) < 2:
        print "NOT enough variables"
    else:
        collections = ["intrapreneuriat"]
        # collections = ["intrapreneuriat", "reconversions"]
        client = MongoClient("mongodb://51.254.223.26:27017")
        db = client.posts

        # create_pickle(argv[1])  # create pickle
        read_pickle(argv[1])
