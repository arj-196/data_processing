import sys


def allocate_memory(max_gb):
    print "Testing memory allocation"

    c = []
    for i in range(1, int(max_gb)):
        try:
            print "allocate", i, "GB"
            b = [1] * (3 * 9 ** 8)  # about 1 GB
            c.append(b)
        except MemoryError:
            print "OUT OF MEMORY"
            del c
            exit(196)
    del c

if __name__ == '__main__':
    if sys.argv[1:]:
        allocate_memory(sys.argv[1])
    else:
        print "Error : Not all params given"
