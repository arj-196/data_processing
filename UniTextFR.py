# -*- coding: utf-8 -*-

################# DEV SOLUTION ################

# Get pare
import os.path

parent_path = os.path.abspath(os.path.join(os.path.dirname(os.path.abspath(__file__)), os.pardir))

import sys

reload(sys)

sys.setdefaultencoding("utf-8")

######################################################
# Class that implements text analysis for french langauge
# Dependencies
# 1. pattern.fr [Clips]
# 2.
from pattern.text.fr import parse, split, sentiment, parsetree, ngrams
import pandas as pd
from collections import Counter
import unicodedata
import re
import pickle
# Import all the modules from main class
# from UniText import UniText
from UniText import *


class UniTextFR(UniText):
    # def __init__(self,corpus,on_sentences=True,no_duplicates=True):
    #	super('UniText',self).__init__(corpus,on_sentences,no_duplicates)

    def io_to_pickle(self, name):

        with open(name + ".pickle", 'wb') as f:
            pickle.dump(self, f)

    def io_print_verbatims(self, keyword, n=0):

        # if number of words is equal to zero it prints all verbatims when a user press a button
        if (n == 0):
            # prints n verbatims
            t = self.corpus[self.corpus['Corpus'].str.contains(keyword)]
            for text in t['Corpus']:
                print text
                raw_input("")
        else:
            # prints n verbatims
            i = 0
            t = self.corpus[self.corpus['Text'].str.contains(keyword)]
            for text in t['Text']:
                i = i + 1
                print text
                if (i == (n - 1)): break

    ##################################### CLEANING #######################################
    def clean_duplicates(self):
        # @todo STEP 2
        # PROBLEM the number of rows in cleaned_corpus must match the number of rows in corpus
        pass

    def clean_ponctuation(self):
        # @todo STEP 2

        self.cleaned_corpus = self.cleaned_corpus.apply(lambda x: x.str.replace(",", " "), axis=1)
        self.cleaned_corpus = self.cleaned_corpus.apply(lambda x: x.str.replace(";", " "), axis=1)
        self.cleaned_corpus = self.cleaned_corpus.apply(lambda x: x.str.replace(":", " "), axis=1)

        print('Removed ponctuation')

    def clean_accents(self):
        # @todo STEP 3
        pass

    def clean_special_chars(self):

        self.cleaned_corpus = self.cleaned_corpus.apply(lambda x: x.str.replace("€", ""), axis=1);
        self.cleaned_corpus = self.cleaned_corpus.apply(lambda x: x.str.replace(">", ""), axis=1);
        self.cleaned_corpus = self.cleaned_corpus.apply(lambda x: x.str.replace("<", ""), axis=1)
        self.cleaned_corpus = self.cleaned_corpus.apply(lambda x: x.str.replace("=", ""), axis=1)
        self.cleaned_corpus = self.cleaned_corpus.apply(lambda x: x.str.replace("m²", ""), axis=1)
        self.cleaned_corpus = self.cleaned_corpus.apply(lambda x: x.str.replace("®", ""), axis=1)
        self.cleaned_corpus = self.cleaned_corpus.apply(lambda x: x.str.replace("»", ""), axis=1)

        print('Removed all the special characters')

    def clean_numbers(self):

        self.cleaned_corpus = self.cleaned_corpus.apply(lambda x: x.str.replace("0", ""), axis=1)
        self.cleaned_corpus = self.cleaned_corpus.apply(lambda x: x.str.replace("1", ""), axis=1)
        self.cleaned_corpus = self.cleaned_corpus.apply(lambda x: x.str.replace("2", ""), axis=1)
        self.cleaned_corpus = self.cleaned_corpus.apply(lambda x: x.str.replace("3", ""), axis=1)
        self.cleaned_corpus = self.cleaned_corpus.apply(lambda x: x.str.replace("4", ""), axis=1)
        self.cleaned_corpus = self.cleaned_corpus.apply(lambda x: x.str.replace("5", ""), axis=1)
        self.cleaned_corpus = self.cleaned_corpus.apply(lambda x: x.str.replace("6", ""), axis=1)
        self.cleaned_corpus = self.cleaned_corpus.apply(lambda x: x.str.replace("7", ""), axis=1)
        self.cleaned_corpus = self.cleaned_corpus.apply(lambda x: x.str.replace("8", ""), axis=1)
        self.cleaned_corpus = self.cleaned_corpus.apply(lambda x: x.str.replace("9", ""), axis=1)

        print('Removed all the numbers')

    def clean_html(self):

        self.cleaned_corpus = self.cleaned_corpus.apply(lambda x: x.str.replace('<[^>]+?/>', "", case=False), axis=1)

        print('Cleaned html')

    def clean_urls(self):
        pass

    def clean_tolower(self):
        # @todo finish
        self.cleaned_corpus = self.cleaned_corpus.apply(lambda x: x.str.lower(), axis=1)

    def clean_words(self, list_words):

        # clean full words that match
        # Figure out why [0] in x.str.split()
        self.cleaned_corpus = self.cleaned_corpus.apply(
            lambda x: ' '.join([word for word in x.str.split()[0] if word not in list_words]), axis=1)

        print('Removed following words : ' + ' '.join(list_words))

    def clean_singular_plural(self, list_nouns):
        # @todo STEP 2
        pass

    def clean_conjugated(self, list_verbs):
        # @todo STEP 2
        pass

    ##################################### EXTRACTION #######################################
    # method to extract part of speech (nouns, verbs, adjs) from cleaned corpus
    def extract_pos(self):
        all_nouns = []
        all_verbs = []
        all_adjs = []
        all_words = []

        JJ = []  # adjectives
        NN = []  # nouns
        VV = []  # verbs

        for i, element in self.cleaned_corpus.iterrows():
            el_nouns = []
            el_verbs = []
            el_adjs = []
            el_words = []

            q = parsetree(element.to_string(), tokenize=True)
            for n in q.sentences:
                try:
                    for noun in n.nouns:
                        el_nouns.append(noun.string)

                    for verb in n.verbs:
                        el_verbs.append(verb.string)

                    for adj in n.adjectives:
                        el_adjs.append(adj.string)

                    for word in n.words:
                        el_words = word.string
                except:
                    pass

            all_nouns.append(" ".join(el_nouns))
            all_verbs.append(" ".join(el_verbs))
            all_adjs.append(" ".join(el_adjs))
            all_words.append(el_words)

            # To be optimized
            for t in n.tagged:

                if t[1].startswith("J"):
                    JJ.append(t[0].lower().encode("utf-8"))
                elif t[1].startswith("V"):
                    VV.append(t[0].lower().encode("utf-8"))
                elif t[1].startswith("N"):
                    NN.append(t[0].lower().encode("utf-8"))

        self.nouns = all_nouns
        self.verbs = all_verbs
        self.adjs = all_adjs
        self.tokens = all_words

        self.count_nouns = Counter(NN).most_common()
        self.count_verbs = Counter(VV).most_common()
        self.count_adjs = Counter(JJ).most_common()

        print('Extracted all POS')

    def count_all_words(self):
        # @todo check
        print "All words : " + str(sum(Counter(" ".join(self.nouns)).values()))

    # method to show n most common words in original corpus by default (cleaned = True to enable the count in cleaned corpus)
    def extract_top_words(self, n, type='All', cleaned=False):
        # Print the results
        if (type == 'All'):
            print 'All is not available yet. Choose Nouns,Verbs or Adjs'
            top_words = []
        elif (type == 'Nouns'):
            top_words = self.count_nouns[1:n]
        elif (type == 'Verbs'):
            top_words = self.count_verbs[1:n]
        elif (type == 'Adjs'):
            top_words = self.count_adjs[1:n]
        else:
            print 'All is not available yet. Choose Nouns,Verbs or Adjs'
            top_words = []

        for word in top_words:
            print word[0] + " : " + str(word[1])

    def extract_ngram_count(self, top_n):
        # Extract the count ngrams and print the results
        # @todo It only works for trigrams
        c = Counter([item[0] for sublist in self.ngrams for item in sublist])
        ngrams = c.most_common(top_n)

        for ngram in ngrams:
            print ngram[0] + " " + ngram[1] + " " + ngram[2] + " : " + str(ngram[1])

    def extract_connotations(self, pos=False):

        if (pos == False):
            pass

    def extract_ngrams(self, n):

        # function ngrams from pattern.fr
        all_ngrams = []

        for i, element in self.cleaned_corpus.iterrows():
            all_ngrams.append(ngrams(element.to_string(), n))

        self.ngrams.append(all_ngrams)

        # @todo Add ngrams count

        print(str(n) + '-grams extracted')

    def extract_similar_sentences(self):
        pass

    def extract_entities(self):
        pass

    def extract_sov_words(self, list_words):

        for word in list_words:
            count = self.tokens.count(word)
            print(word + " : " + str(count))

        print ("SOV extracted")
