# -*- coding: utf-8 -*-
import resources as rs
import csv
import counter as dp
import hashlib as hl


class Mapper(object):
    map = {}
    set_ids = {}
    ids_name = {}
    _if_remove_stop_words = None
    lang = 'fr'

    def __init__(self, remove_stop_words):
        self._if_remove_stop_words = remove_stop_words

    @staticmethod
    def _clean_text(text):
        return text.lower() \
            .replace(",", " ") \
            .replace(";", " ") \
            .replace(":", " ") \
            .replace("\"", " ")

    @staticmethod
    def _tokenize_text(text):
        return text.split()

    @staticmethod
    def _remove_stop_words(tokens, lang):
        cleaned_tokens = []
        for token in tokens:
            status = True
            for word in rs.stop_words[lang]:
                if token == word:
                    status = False
                    break

            if status:
                cleaned_tokens.append(token)
        return cleaned_tokens

    @staticmethod
    def _load_csv_file(category_to_rs):
        data = []
        with open('data/' + category_to_rs[0], 'rb') as csvfile:
            spamreader = csv.reader(csvfile, delimiter=category_to_rs[1], quotechar='|')
            for row in spamreader:
                data.append(row)
        return data


class StringMapper(Mapper):
    # TODO when split=False map variable has duplicate entries
    def __init__(self, data, id_index, value_index, name_index, initial_value_type, remove_stop_words=True,
                 split=True):
        super(StringMapper, self).__init__(remove_stop_words)
        self._create_map(data, id_index, value_index, name_index, initial_value_type, split)

    def _create_map(self, data, id_index, value_index, name_index, initial_value_type, split):
        if isinstance(data, list):
            for d in data:
                # building token to id_index(es) map
                text = self._clean_text(d[value_index])
                if split:
                    tokens = self._tokenize_text(text)
                else:
                    tokens = [text]
                if self._if_remove_stop_words:
                    cleaned_tokens = self._remove_stop_words(tokens, self.lang)
                else:
                    cleaned_tokens = tokens

                for token in cleaned_tokens:
                    cleaned_token = self._clean_text(token)
                    if cleaned_token not in self.map:
                        self.map[cleaned_token] = [d[id_index]]
                    else:
                        self.map[cleaned_token].append(d[id_index])

                # building id_index map
                if d[id_index] not in self.set_ids:
                    self.set_ids[d[id_index]] = {
                        "value": initial_value_type,
                        "cause": []
                    }

                # building name_index map
                if d[id_index] not in self.ids_name:
                    self.ids_name[d[id_index]] = d[name_index]


class NGramMapper(Mapper):
    id_gram_to_gram_map = {}
    gram_to_length_map = {}

    def __init__(self, data, id_index, value_index, name_index, initial_value_type, remove_stop_words=True,
                 split=True):
        super(NGramMapper, self).__init__(remove_stop_words)
        self._create_map(data, id_index, value_index, name_index, initial_value_type)

    def _create_map(self, data, id_index, value_index, name_index, initial_value_type):
        if isinstance(data, list):
            for d in data:
                tokens = self._tokenize_text(self._clean_text(d[value_index]))
                # TODO remove stop words or not ? depends on if mike removes stop words

                # get set of 2-4 length ngrams
                ngram_set = dp.Utils.get_ngrams_set(tokens)

                for grams in ngram_set:
                    for gram in grams:
                        id_gram = self.hash_ngram(gram)

                        # building self.map
                        if id_gram not in self.map:
                            self.map[id_gram] = [d[id_index]]
                        else:
                            self.map[id_gram].append(d[id_index])

                        # building self.id_gram_to_gram_map
                        if id_gram not in self.id_gram_to_gram_map:
                            self.id_gram_to_gram_map[id_gram] = [gram]
                        else:
                            self.id_gram_to_gram_map[id_gram].append(gram)

                        # building self.gram_to_length_map
                        if id_gram not in self.gram_to_length_map:
                            self.gram_to_length_map[id_gram] = len(gram)

                # building name_index map
                if d[id_index] not in self.ids_name:
                    self.ids_name[d[id_index]] = d[name_index]

                # building set_ids map
                if d[id_index] not in self.set_ids:
                    self.set_ids[d[id_index]] = {
                        "value": initial_value_type,
                        "gram_value": {},
                        "cause": {}
                    }

    @staticmethod
    def hash_ngram(gram):
        return hl.md5("".join(gram)).hexdigest()


class MapperCollection(object):
    # TODO cannot handle more than a single mapper at moment
    @staticmethod
    def create_mappers(type_mapper, categories, categories_csv_mapper, categories_to_resource_map, split=True):
        print "Init Mappers"
        mappers = []
        for category in categories:
            print "---------", "Category : ", category

            # load category data
            data = Mapper._load_csv_file(categories_to_resource_map[category])
            # create mapper
            mapper = type_mapper(data, categories_csv_mapper[category][0], categories_csv_mapper[category][1],
                                 categories_csv_mapper[category][2], 0, remove_stop_words=False, split=split)
            # TODO solve mutable  problem here,
            mappers.append({"mapper": mapper, "values": mapper.set_ids, "category": category})
        return mappers
