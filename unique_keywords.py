import resources as rs
import mapper as mp
from pymongo import MongoClient
import re
from datetime import datetime
import sys, traceback
import os
from debugger.debugger import Debugger
from pattern.vector import Document
from sets import Set

startTime = datetime.now()


class UniqueKeywordsCounter(object):
    clean_cache_interval = 20000
    iteration = 0
    db_grams = None  # database to store gram frequencies
    map = None  # hash index to frequency
    cache = None  # temporary hash index to gram
    debug = ""

    def __init__(self, db_gram, clean_cache=True):
        self.db_grams = db_gram  # defining dbase
        self.map = {}
        self.cache = {}
        self.if_clean_cache = clean_cache

    def __del__(self):
        print "UNIQUE GRAM COUNTER : stopped at ", self.debug
        # self.clean_cache()
        # self.remove_inrelevant()

    @staticmethod
    def clean_sentence(sentence):
        # to lower
        s = sentence.lower()
        # remove all puntutations
        for p in rs.puntuations:
            s = s.replace(p, " ")
        # remove multiple spaces
        s = re.sub('\d', '', s)
        s = re.sub(' +', ' ', s)
        return s

    def run(self, collections, _type="mongo"):
        print "UNIQUE KEYWORD COUNTER : Start run"

        if _type == "mongo":
            db = client.bnp_ent
            for collection in collections:
                print "-- collection ", collection
                for thread in db[collection].find({"posts": {"$exists": "true", "$not": {"$size": 0}}},
                                                  {"_id": 0, "posts": 1}):
                    if 'posts' in thread:
                        for post in thread['posts']:
                            self.debug = collection + " " + str(self.iteration)
                            self.iteration += 1
                            if 'content' in post:
                                content = self.clean_sentence(post['content'])
                                tokens = content.split(' ')
                                self._get_keywords(tokens)

                # clean cache at interval
                if self.iteration % self.clean_cache_interval == 0:
                    self.clean_cache()

        if _type == "file":
            SEP = '\t'
            NEWLINE = '\n'
            for collection in collections:
                print "-- collection", collection.name
                lines = collection.read()
                for row in lines.split(NEWLINE):
                    self.iteration += 1
                    self.debug = collection.name + " " + str(self.iteration)
                    try:
                        content = self.clean_sentence(row.decode('utf-8'))
                        self._get_keywords(row.decode('utf-8'))
                    except:
                        e = sys.exc_info()[0]
                        print ("ERROR  %s", e)
                        traceback.print_exc(file=sys.stdout)

                # clean cache at interval
                if self.iteration % self.clean_cache_interval == 0:
                    self.clean_cache()

                    #
                    # test break condition
                    # if self.iteration > 5:
                    #     print "TEST: PREMATURE BREAK"
                    #     exit(196)
        print "UNIQUE KEYWORD COUNTER : End run", datetime.now() - startTime

    @staticmethod
    def is_number(s):
        try:
            float(s)
            return True
        except ValueError:
            try:
                int(s)
            except ValueError:
                return False

    def _clean_keywords(self, keywords):
        filteredlist = []
        for t in keywords:
            status = True
            for st in rs.stop_words['fr']:
                if t[1] == st:
                    status = False
                    break
            if self.is_number(t[1]):
                status = False

            if status:
                filteredlist.append(t)
        return filteredlist

    def _get_keywords(self, text):
        d = Document(text)
        # # TODO enhancement, use Document to extract more relevant keywords
        # extract keywords from text
        keywords = self._clean_keywords(d.keywords())
        for keyword in keywords:
            self._analyze_gram(keyword)

    def _analyze_gram(self, keyword):
        id_keyword = mp.NGramMapper.hash_ngram(keyword[1])

        # update map
        if id_keyword not in self.map:
            self.map[id_keyword] = keyword[0]
        else:
            self.map[id_keyword] += keyword[0]

        # update cache
        if id_keyword not in self.cache:
            self.cache[id_keyword] = keyword[1]

    def clean_cache(self):
        if self.if_clean_cache:
            print "UNIQUE KEYWORD COUNTER : Clear Cache", datetime.now() - startTime
            col = self.db_grams["master"]
            for id_keyword in self.cache:
                if self.map[id_keyword] > threshold:
                    # check in db
                    item = col.find_one({"_id": id_keyword})
                    if not item:
                        col.insert({
                            "_id": id_keyword,
                            "frequency": self.map[id_keyword],
                            "gram": self.cache[id_keyword]
                        })
                    else:
                        new_freq = self.map[id_keyword] + int(item['frequency'])
                        col.update_one(
                            {"_id": id_keyword},
                            {
                                "$set": {
                                    "frequency": new_freq
                                }
                            }
                        )

        # clear cache and gram_length_list
        self.cache = {}

    def remove_inrelevant(self):
        print "UNIQUE KEYWORD COUNTER : Delete Unrelevant", datetime.now() - startTime
        for l in range(rs.MIN_LENGTH_GRAMS, rs.MAX_LENGTH_GRAMS + 1):
            print "length_" + str(l), {"frequency": {"$lt": str(threshold[l])}}, datetime.now() - startTime
            self.db_grams["length_" + str(l)].remove({"frequency": {"$lt": threshold[l]}})

    def export_results(self, filename):
        file_suffix = '.csv'
        sep = '\t'
        newline = '\n'
        print "UNIQUE KEYWORD COUNTER : Export"
        col_name = "master"
        f = open(filename + col_name + file_suffix, 'w')
        f.close()
        f = open(filename + col_name + file_suffix, 'a')
        for o in self.db_grams[col_name].find():
            string = ""
            string += o['gram'] + sep
            string += str(o['frequency']) + newline
            f.write(string.encode("utf8", "ignore"))
        f.close()

    def export_results_by_category(self, categories, filename):
        # TODO
        file_suffix = '.csv'
        sep = '\t'
        newline = '\n'
        print "UNIQUE KEYWORD COUNTER : Export"
        for category in categories.keys():
            print "--CATEGORY", category
            visited_ids = []
            _list = categories[category]
            for l in range(rs.MIN_LENGTH_GRAMS, rs.MAX_LENGTH_GRAMS + 1):
                print "length_" + str(l)
                col_name = "length_" + str(l)
                f = open(filename + category + "_" + col_name + file_suffix, 'w')
                f.close()
                f = open(filename + category + "_" + col_name + file_suffix, 'a')
                for e in _list:
                    tokens = self.clean_sentence(e).encode('utf-8').split(' ')  # split on whitespace
                    for o in self.db_grams[col_name].find(
                            {"gram": {"$size": l, "$all": tokens}, "_id": {"$nin": visited_ids}}):
                        visited_ids.append(o["_id"])
                        string = ""
                        string += " ".join(o['gram']) + sep
                        string += str(o['frequency']) + newline
                        f.write(string.encode("utf8", "ignore"))
                f.close()


# ---
# define threshold
#
threshold = 0
client = MongoClient('mongodb://localhost:27018')
db_grams = client.keywords_clusters_c  # choosing db


def custom():
    # cluster 0
    counter = UniqueKeywordsCounter(client.keywords_clusters_c_0, clean_cache=True)  # choosing db
    # tsv files
    _dir = "verbatims/backup/c/0/"
    files = []
    for _file in os.listdir(_dir):
        if os.path.isfile(_dir + _file):
            files.append(open(_dir + _file, 'r'))
    counter.run(files, _type="file")
    counter.clean_cache()
    counter.export_results("results/unique_keywords_cluster_0_")

    # cluster 1
    counter = UniqueKeywordsCounter(client.keywords_clusters_c_1, clean_cache=True)  # choosing db
    # tsv files
    _dir = "verbatims/backup/c/1/"
    files = []
    for _file in os.listdir(_dir):
        if os.path.isfile(_dir + _file):
            files.append(open(_dir + _file, 'r'))
    counter.run(files, _type="file")
    counter.clean_cache()
    counter.export_results("results/unique_keywords_cluster_1_")

    # cluster 2
    counter = UniqueKeywordsCounter(client.keywords_clusters_c_2, clean_cache=True)  # choosing db
    # tsv files
    _dir = "verbatims/backup/c/2/"
    files = []
    for _file in os.listdir(_dir):
        if os.path.isfile(_dir + _file):
            files.append(open(_dir + _file, 'r'))
    counter.run(files, _type="file")
    counter.clean_cache()
    counter.export_results("results/unique_keywords_cluster_2_")

    # cluster 3
    counter = UniqueKeywordsCounter(client.keywords_clusters_c_3, clean_cache=True)  # choosing db
    # tsv files
    _dir = "verbatims/backup/c/3/"
    files = []
    for _file in os.listdir(_dir):
        if os.path.isfile(_dir + _file):
            files.append(open(_dir + _file, 'r'))
    counter.run(files, _type="file")
    counter.clean_cache()
    counter.export_results("results/unique_keywords_cluster_3_")

    # cluster 4
    counter = UniqueKeywordsCounter(client.keywords_clusters_c_4, clean_cache=True)  # choosing db
    # tsv files
    _dir = "verbatims/backup/c/4/"
    files = []
    for _file in os.listdir(_dir):
        if os.path.isfile(_dir + _file):
            files.append(open(_dir + _file, 'r'))
    counter.run(files, _type="file")
    counter.clean_cache()
    counter.export_results("results/unique_keywords_cluster_4_")


def main():
    counter = UniqueKeywordsCounter(db_grams)

    ### mongo
    # counter.run(['bnpdb2', 'bnpdb3', 'bnpdb4', 'bnpdb5'], type="mongo")

    ### tsv files
    _dir = "verbatims/backup/c/"
    files = []
    for _file in os.listdir(_dir):
        if os.path.isfile(_dir + _file):
            files.append(open(_dir + _file, 'r'))

    counter.run(files, _type="file")


def export(filename):
    counter = UniqueKeywordsCounter(db_grams, clean_cache=False)
    counter.export_results("results/unique_grams_" + filename + '_')


def export_categories(filename):
    counter = UniqueKeywordsCounter(db_grams, clean_cache=False)
    counter.export_results_by_category(rs.KEYLIST_TOPICS, "results/unique_grams_categories_" + filename + "_")


if __name__ == '__main__':
    argv = sys.argv
    if argv[1] == 'calculate':
        main()
    if argv[1] == 'custom':
        custom()
    if argv[1] == 'export':
        export(argv[2])

    if argv[1] == 'exportcategories':
        export_categories(argv[2])

#
# python unique_grams calculate
# python unique_grams export _filename
# python unique_grams exportcategories _filename
#
